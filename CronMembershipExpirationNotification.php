<?php

require 'DBConn.php';
require 'Mail.php';

$sql = 'select expired_at, datediff(expired_at, now()) day_left, name, email
        from g_registration
        where datediff(expired_at, now()) = 7 and active = true ';

$stmt = $pdo->query($sql);
$res = $stmt->fetchAll();

foreach ($res as $row) {
    $recipientEmail = $row['email'];
    $recipientName = $row['name'];
    $expirationDate = $row['expired_at'];
    $mail = new Mail(
        $recipientEmail,
        $recipientName,
        'Your membership is ending soon',
        "");
    $mail->renderMailTemplate('mail-template/tpl-04.html',
        ['$name$' => $recipientName, '$expirationDate$' => date('d/m/y', strtotime($expirationDate))]);
    $mail->send();
}

?>

