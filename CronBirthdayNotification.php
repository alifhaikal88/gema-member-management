<?php

require 'DBConn.php';
require 'Mail.php';

$sql = 'select name, email, year(now())- year(dob) age
from g_registration
where extract(day from dob) = extract(day from now()) 
    and extract(month from dob) = extract(month from now()) 
    and active = true ';

$stmt = $pdo->query($sql);
$res = $stmt->fetchAll();

foreach ($res as $row) {
    $recipientEmail = $row['email'];
    $recipientName = $row['name'];
    $recipientAge = $row['age'];
    $mail = new Mail(
        $recipientEmail,
        $recipientName,
        'Happy ' . $recipientAge . 'th birthday to you!',
        "");
    $mail->renderMailTemplate('mail-template/tpl-03.html', Array('$name$' => $recipientName));
    $mail->send();
}

?>

