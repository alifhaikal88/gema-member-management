DROP TABLE IF EXISTS g_registration;
DROP TABLE IF EXISTS g_configuration;
DROP TABLE IF EXISTS g_race;
DROP TABLE IF EXISTS g_religion;
DROP TABLE IF EXISTS g_branch;
DROP TABLE IF EXISTS g_renewal;
DROP TABLE IF EXISTS t_staff;
DROP TABLE IF EXISTS logins;

CREATE TABLE g_registration (
  id               INT                 AUTO_INCREMENT PRIMARY KEY,
  registration_no  VARCHAR(100)  NULL,
  name             VARCHAR(100)  NULL,
  age              INT           NULL,
  dob              DATE          NULL,
  nric             VARCHAR(100)  NULL,
  branch           VARCHAR(100)  NULL,
  email            VARCHAR(100)  NULL,
  phone            VARCHAR(100)  NULL,
  mobile           VARCHAR(100)  NULL,
  address          VARCHAR(1000) NULL,
  city             VARCHAR(100)  NULL,
  state            VARCHAR(100)  NULL,
  postcode         VARCHAR(100)  NULL,
  employer_address VARCHAR(1000) NULL,
  education        VARCHAR(100)  NULL,
  occupation       VARCHAR(100)  NULL,
  race             VARCHAR(100)  NULL,
  ext_race         VARCHAR(100)  NULL,
  religion         VARCHAR(100)  NULL,
  ext_religion     VARCHAR(100)  NULL,
  gender           VARCHAR(100)  NULL,
  interest         VARCHAR(3000) NULL,
  membership_type  CHAR          NULL  DEFAULT '0',
  bill_id          VARCHAR(10)   NULL,
  status           VARCHAR(50)   NULL, -- pending, approve
  paid             BOOLEAN       NULL  DEFAULT FALSE,
  expired_at       DATE,
  renew_at         DATE,
  active           BOOLEAN             DEFAULT FALSE,
  approve_date     DATE,
  cancel_date      DATE,
  c_ts             TIMESTAMP     NULL,
  m_ts             TIMESTAMP     NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1;

CREATE TABLE g_renewal (
  id              INT                 AUTO_INCREMENT PRIMARY KEY,
  nric            VARCHAR(1000) NULL,
  renew_date      DATE          NULL,
  membership_type CHAR          NULL  DEFAULT '0',
  bill_id         VARCHAR(10)   NULL,
  paid            BOOLEAN       NULL  DEFAULT FALSE,
  c_ts            TIMESTAMP     NULL,
  m_ts            TIMESTAMP     NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1;

CREATE TABLE g_configuration (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  k    VARCHAR(1000) NULL,
  v    VARCHAR(1000) NULL,
  c_ts TIMESTAMP     NULL,
  m_ts TIMESTAMP     NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1;

INSERT INTO g_configuration (k, v, c_ts)
VALUES ('memberRegistrationNo', '1000', now());
INSERT INTO g_configuration (k, v, c_ts)
VALUES ('membershipNormalAmount', '10', now());
INSERT INTO g_configuration (k, v, c_ts)
VALUES ('membershipFullAmount', '50', now());


CREATE TABLE g_race (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1;

INSERT INTO g_race (name)
VALUES ('MELAYU');
INSERT INTO g_race (name)
VALUES ('CINA');
INSERT INTO g_race (name)
VALUES ('INDIA');

CREATE TABLE g_religion (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1;

INSERT INTO g_religion (name)
VALUES ('ISLAM');
INSERT INTO g_religion (name)
VALUES ('BUDDHA');
INSERT INTO g_religion (name)
VALUES ('KRISTIAN');
INSERT INTO g_religion (name)
VALUES ('HINDU');
INSERT INTO g_religion (name)
VALUES ('SIKH');

CREATE TABLE g_branch (
  id                    INT AUTO_INCREMENT PRIMARY KEY,
  name                  VARCHAR(100) NOT NULL,
  billplz_secret_key    VARCHAR(100),
  billplz_collection_id VARCHAR(100)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1;

INSERT INTO g_branch (name)
VALUES ('PERLIS');
INSERT INTO g_branch (name)
VALUES ('KEDAH');
INSERT INTO g_branch (name, billplz_secret_key)
VALUES ('PULAU PINANG', '7343b309-f836-4400-b93f-fb11b1e0354f');
INSERT INTO g_branch (name)
VALUES ('PERAK');
INSERT INTO g_branch (name)
VALUES ('SELANGOR');
INSERT INTO g_branch (name)
VALUES ('KUALA LUMPUR');
INSERT INTO g_branch (name)
VALUES ('NEGERI SEMBILAN');
INSERT INTO g_branch (name)
VALUES ('MELAKA');
INSERT INTO g_branch (name)
VALUES ('JOHOR');
INSERT INTO g_branch (name)
VALUES ('PAHANG');
INSERT INTO g_branch (name)
VALUES ('TERENGGANU');
INSERT INTO g_branch (name)
VALUES ('KELANTAN');
INSERT INTO g_branch (name)
VALUES ('SABAH');
INSERT INTO g_branch (name)
VALUES ('SARAWAK');

UPDATE g_branch
SET billplz_secret_key = '2e7c89a1-56ff-43af-a729-5b283267bdd3';
UPDATE g_branch
SET billplz_collection_id = '6kab4fbn';

-- auto-generated definition
CREATE TABLE t_staff
(
  id          INT AUTO_INCREMENT PRIMARY KEY,
  username    VARCHAR(20)
              COLLATE latin1_general_ci NOT NULL,
  password    VARCHAR(20)
              COLLATE latin1_general_ci NOT NULL,
  staff_name  VARCHAR(100)
              COLLATE latin1_general_ci NOT NULL,
  staff_ic    VARCHAR(12)
              COLLATE latin1_general_ci NOT NULL,
  staff_sex   VARCHAR(10)
              COLLATE latin1_general_ci NOT NULL,
  staff_add   VARCHAR(200)
              COLLATE latin1_general_ci NOT NULL,
  staff_post  VARCHAR(5)
              COLLATE latin1_general_ci NOT NULL,
  staff_town  VARCHAR(30)
              COLLATE latin1_general_ci NOT NULL,
  staff_state VARCHAR(12)
              COLLATE latin1_general_ci NOT NULL,
  staff_tel   VARCHAR(40)
              COLLATE latin1_danish_ci  NOT NULL,
  staff_email VARCHAR(100)
              COLLATE latin1_danish_ci  NOT NULL,
  staff_cat   VARCHAR(10)
              COLLATE latin1_danish_ci  NOT NULL,
  user_type   VARCHAR(1)
              COLLATE latin1_danish_ci  NOT NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;

INSERT INTO t_staff (id,
                     username,
                     password,
                     staff_name,
                     staff_ic,
                     staff_sex,
                     staff_add,
                     staff_post,
                     staff_town,
                     staff_state,
                     staff_tel,
                     staff_email,
                     staff_cat,
                     user_type)
VALUES ('1', 'admin', 'admin', 'admin', '870110749556', '', '', 'hjgd7', '', '', '', 'admin@gmail.com', 'Admin', '1');

