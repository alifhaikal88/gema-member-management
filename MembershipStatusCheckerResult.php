<?php

require "DBConn.php";

//$nric = '870101045567';
$nric = $_POST['nric'];

$stmt = $pdo->prepare('select * from g_registration g where g.nric = :nric and g.paid = true');
$stmt->bindValue(':nric', $nric);
$stmt->execute();
$result = $stmt->fetchAll();
$count = count($result);

echo json_encode(['count' => $count, 'result' => $result]);
