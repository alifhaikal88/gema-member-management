<?php
require dirname(__FILE__) . '/../admin/inc/_global/config.php';
require dirname(__FILE__) . '/../admin/inc/backend/config.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_start.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_end.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../admin/../DBConn.php';
require dirname(__FILE__) . '/../admin/../services/CommonService.php';

global $commonService;

if (isset($_GET) && isset($_GET['id'])) {
    $interest = $commonService->getInterestById($_GET['id']);
}

if (isset($_POST)) {
    if (isset($_POST['submit'])) {
        $newInterest = [
            'id' => $_GET['id'],
            'name' => strtoupper($_POST['name']),
        ];

        $commonService->updateInterest($newInterest);
        echo "<script>window.location.href = 'InterestList.php'</script>";

    } else if (isset($_POST['delete'])) {
        $commonService->deleteInterest(array('id' => $_GET['id']));
        echo "<script>window.location.href = 'InterestList.php'</script>";
    }
}

?>

    <div class="content">
        <h2 class="content-heading">Minat</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Kemaskini Minat</h3>
            </div>
            <div class="block-content block-content-full">
                <form class="settings-form" action="#" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="name"
                                       name="name"
                                       value="<? echo $interest['name'] ?>">
                                <label for="name">Nama</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" name="submit" class="btn btn-alt-info">
                                <i class="fa fa-save mr-5"></i> Save
                            </button>
                            <button type="submit" name="delete" class="btn btn-alt-warning">
                                <i class="fa fa-remove mr-5"></i> Delete
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Container -->
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/page_end.php'; ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/pages/race-form-validation.js'); ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_end.php'; ?>