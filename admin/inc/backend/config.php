<?php
/**
 * backend/config.php
 *
 * Author: pixelcave
 *
 * Codebase - Backend papges configuration file
 *
 */

// **************************************************************************************************
// BACKEND INCLUDED VIEWS
// **************************************************************************************************

//                              : Useful for adding different sidebars/headers per page or per section
$cb->inc_side_overlay = dirname(__FILE__) . '/views/inc_side_overlay.php';
$cb->inc_sidebar = dirname(__FILE__) . '/views/inc_sidebar.php';
$cb->inc_header = dirname(__FILE__) . '/views/inc_header.php';
$cb->inc_footer = dirname(__FILE__) . '/views/inc_footer.php';


// **************************************************************************************************
// BACKEND MAIN MENU
// **************************************************************************************************

// You can use the following array to create your main menu
$cb->main_nav = array(
    array(
        'name' => '<span class="sidebar-mini-hide">Laman Utama</span>',
        'icon' => 'si si-compass',
        'url' => 'index.php'
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Permohonan Manual</span>',
        'icon' => 'si si-docs',
        'url' => 'NewRegistration.php'
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Senarai Ahli</span>',
        'icon' => 'si si-people',
        'sub' => array(
            array(
                'name' => 'Belum diluluskan',
                'url' => 'ApplicationList.php'
            ),
            array(
                'name' => 'Aktif',
                'url' => 'ActiveMemberList.php'
            ),
            array(
                'name' => 'Tidak Aktif',
                'url' => 'NotActiveMemberList.php'
            ),
            array(
                'name' => 'Alumni',
                'url' => 'AlumniMemberList.php'
            ),
            array(
                'name' => 'Ditolak',
                'url' => 'RejectMemberList.php'
            )
        )
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Pengurusan</span>',
        'icon' => 'si si-people',
        'sub' => array(
            array(
                'name' => 'Daftar Staf',
                'url' => 'StaffNew.php'
            ),
            array(
                'name' => 'Senarai Staf',
                'url' => 'StaffList.php'
            )
        )
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Data Asas</span>',
        'icon' => 'si si-list',
        'url' => 'data.php',
        'sub' => array(
            array(
                'name' => 'Agama',
                'url' => 'ReligionList.php'
            ),
            array(
                'name' => 'Bangsa',
                'url' => 'RaceList.php'
            ),
            array(
                'name' => 'Cawangan',
                'url' => 'BranchList.php'
            ),
            array(
                'name' => 'Pekerjaan',
                'url' => 'OccupationList.php'
            ),
            array(
                'name' => 'Minat',
                'url' => 'InterestList.php'
            )
        )

    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Konfigurasi</span>',
        'icon' => 'si si-settings',
        'url' => 'Settings.php'
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Laporan (Chart)</span>',
        'icon' => 'si si-handbag',
        'sub' => array(
            array(
                'name' => 'Jantina',
                'url' => 'GenderChart.php'
            ),
            array(
                'name' => 'Cawangan',
                'url' => 'BranchChart.php'
            ),
            array(
                'name' => 'Bangsa',
                'url' => 'RaceChart.php'
            ),
            array(
                'name' => 'Jenis Kehlian',
                'url' => 'MembershipTypeChart.php'
            ),
            array(
                'name' => 'Status Kehlian',
                'url' => 'MembershipStatusChart.php'
            ),
            array(
                'name' => 'Export to Excel',
                'url' => 'ExcelReport.php'
            )
        )
    )
);