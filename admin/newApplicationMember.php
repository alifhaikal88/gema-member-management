<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/backend/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>

    <!-- Page JS Plugins CSS -->
<?php $cb->get_css('js/plugins/select2/select2.min.css'); ?>
<?php $cb->get_css('js/plugins/select2/select2-bootstrap.min.css'); ?>

<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>

<?php
$id = $_GET['id'];
$query1 = "SELECT * FROM g_registration WHERE id ='$id'";
$result1 = mysqli_query($conn, $query1) or die ('Data application cannot be reached.' . mysqli_error($conn));
$record1 = mysqli_fetch_array($result1);
$_SESSION['id'] = $record1["id"];

?>

    <!-- Page Content -->
    <div class="content">
        <!-- Bootstrap Forms Validation -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Maklumat Pemohon</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option">
                        <i class="si si-wrench"></i>
                    </button>
                </div>
            </div>
            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-6">
                        <!-- jQuery Validation (.js-validation-bootstrap class is initialized in js/pages/be_forms_validation.js) -->
                        <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                        <form id="sign-up-form" class="js-validation-signup" action="processAcceptApplication.php"
                              method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["name"]; ?>" readonly="">
                                        <label for="fullname">Nama Penuh</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" value="<? echo $record1["nric"]; ?>"
                                               readonly="">
                                        <label for="nric">No. Kad Pengenalan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["branch"]; ?>" readonly="">
                                        <label for="fullname">Cawangan Gema</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" value="<? echo $record1["email"]; ?>"
                                               readonly="">
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" readonly=""
                                               value="<? echo $record1["phone"]; ?>">
                                        <label for="phone">No Telefon</label>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" value="<? echo $record1["mobile"]; ?>"
                                               readonly="">
                                        <label for="mobile">No Hp</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["address"]; ?>" readonly="">
                                        <label for="address">Alamat surat menyurat</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["city"]; ?>" readonly="">
                                        <label for="city">Bandar</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["state"]; ?>" readonly="">
                                        <label for="city">Negeri</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" value="<? echo $record1["postcode"]; ?>"
                                               readonly="">
                                        <label for="postcode">Poskod</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["education"]; ?>" readonly="">
                                        <label for="fullname">Taraf Pendidikan</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["occupation"]; ?>" readonly="">
                                        <label for="fullname">Pekerjaan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["race"]; ?>" readonly="">
                                        <label for="city">Bangsa</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["religion"]; ?>" readonly="">
                                        <label for="city">Agama</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" value="LELAKI" readonly="">
                                        <label for="postcode">Jantina</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="<? echo $record1["interest"]; ?>" readonly="">
                                        <label for="address">Minat yang diminati</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="YURAN ASAS" readonly="">
                                        <label for="fullname">Jenis Keahlian</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="2 OGOS 2018" readonly="">
                                        <label for="fullname">Tarikh Mohon</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6">
                                    <div class="form-material floating">
                                        <input type="text" style="text-transform:uppercase" class="form-control"
                                               value="Dalam Proses" readonly="">
                                        <label for="fullname">Status Keahlian</label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-alt-primary">Terima</button>
                                <button type="" class="btn btn-alt-danger">Tolak</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!-- Bootstrap Forms Validation -->
    </div>
    <!-- END Page Content -->

<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/select2/select2.full.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-validation/additional-methods.min.js'); ?>

    <!-- Page JS Code -->
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
    </script>
<?php $cb->get_js('js/pages/be_forms_validation.js'); ?>

<?php require 'inc/_global/views/footer_end.php'; ?>