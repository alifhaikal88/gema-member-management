<?php

session_start();

require dirname(__FILE__) . '/../services/RegistrationService.php';

global $registrationService;

if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../Login.php');
}

if (isset($_POST['update'])) {
    $registrationService->updateRegistration($_SESSION['id']);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

?>