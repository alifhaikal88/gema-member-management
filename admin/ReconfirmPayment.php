<?php

session_start();

require dirname(__FILE__) . '/../services/RegistrationService.php';

global $registrationService;

if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../Login.php');
}

$registrationService->reconfirmPayment($_SESSION['id']);

echo "<script>alert('Keahlian telah diterima');";
print "window.location='ApplicationDetail.php?id=".$_SESSION['id']."'";
print "</script> ";

?>
