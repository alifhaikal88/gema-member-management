<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/backend/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<?php $cb->get_css('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>
<?php $cb->get_css('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'); ?>
<?php $cb->get_css('js/plugins/select2/select2.min.css'); ?>
<?php $cb->get_css('js/plugins/select2/select2-bootstrap.min.css'); ?>
<?php $cb->get_css('js/plugins/jquery-tags-input/jquery.tagsinput.min.css'); ?>

<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>
<?php require dirname(__FILE__) . '/../admin/../DBConn.php';?>
<?php ///require dirname(__FILE__) . '/../admin/../services/CommonService.php';?>
<?php require dirname(__FILE__) . '/../admin/../services/RegistrationService.php';?>
<?php 
global $commonService;
global $registrationService;

if (isset($_POST) && isset($_POST['submit'])) {    

    $registrationService->saveRegistrationFromAdmin($newManualRegistration);
    echo "<script>window.location.href = 'ApplicationList.php'</script>";
}

?>

<!-- Page Content -->
    <div class="">
        <div class="hero-static content content-full bg-white" data-toggle="appear">

            <!-- Header -->
            <div class="py-30 px-5 text-center">                
                <h1 class="h4 font-w700 mt-50 mb-10">Borang Permohonan Keahlian GEMA</h1>
                <h2 class="h5 font-w400 text-muted mb-0">Ruangan dengan tanda * wajib diisi.</h2>
            </div>
            <!-- END Header -->

            <!-- Sign Up Form -->
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">

                    <form id="sign-up-form" class="js-validation-signup" action="#" method="post"
                          enctype="application/x-www-form-urlencoded">
                        <div class="form-group row">
                        </div>
                        <legend class="h5 font-w700 mt-50 mb-10">Maklumat Pemohon</legend>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="nomember"
                                           name="nomember">
                                    <label for="nomember">No. Ahli *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <input type="text" class="js-datepicker form-control" id="registrationDate" name="registrationDate" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="">
                                    <label for="registrationDate">Tarikh Daftar *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <input type="text" class="js-datepicker form-control" id="expiredDate" name="expiredDate" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="">
                                    <label for="expiredDate">Tarikh Tamat Tempoh *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material floating">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="fullname"
                                           name="fullname">
                                    <label for="fullname">Nama Penuh *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-8">
                                <div class="form-material floating">
                                    <input type="text" class="form-control"
                                           onkeydown="return ( event.ctrlKey || event.altKey ||
                                           (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) ||
                                            (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) ||
                                             (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )"
                                           id="nric" name="nric" minlength="12" maxlength="12"
                                           value="<? echo $_GET['nric'] ?>"
                                        <? if ($_GET['nric']) echo 'readonly'; else echo ''; ?>>
                                    <label for="nric">No. Kad Pengenalan *</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-material floating open">
                                    <input type="text" class="form-control"
                                           id="age" name="age" readonly>
                                    <label for="age">Umur</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material floating">
                                <select class="js-select2 form-control" id="branch" name="branch"
                                        style="width: 100%;" data-placeholder="">
                                    <option></option>
                                    <?php
                                    global $commonService;
                                    $religions = $commonService->getBranches();
                                    foreach ($religions as $i) {
                                        echo '<option value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                                <label for="branch">Cawangan Gema *</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material floating">
                                    <input type="email" class="form-control" id="email" name="email">
                                    <label for="email">Email *</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-material floating">
                                    <input type="text" class="form-control" id="phone" name="phone">
                                    <label for="phone">No Telefon</label>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <div class="form-material floating">
                                    <input type="text" class="form-control" id="mobile" name="mobile">
                                    <label for="mobile">No Hp *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material floating">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="address"
                                           name="address">
                                    <label for="address">Alamat surat menyurat *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="city"
                                           name="city">
                                    <label for="city">Bandar *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="state"
                                            name="state">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <option value="PERLIS">PERLIS</option>
                                        <option value="KEDAH">KEDAH</option>
                                        <option value="PULAU PINANG">PULAU PINANG</option>
                                        <option value="PERAK">PERAK</option>
                                        <option value="SELANGOR">SELANGOR</option>
                                        <option value="KUALA LUMPUR">KUALA LUMPUR</option>
                                        <option value="NEGERI SEMBILAN">NEGERI SEMBILAN</option>
                                        <option value="MELAKA">MELAKA</option>
                                        <option value="JOHOR">JOHOR</option>
                                        <option value="PAHANG">PAHANG</option>
                                        <option value="TERENGGANU">TERENGGANU</option>
                                        <option value="KELANTAN">KELANTAN</option>
                                        <option value="SABAH">SABAH</option>
                                        <option value="SARAWAK">SARAWAK</option>
                                    </select>
                                    <label for="state">Negeri *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <input type="text"
                                           onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )"
                                           class="form-control"
                                           maxlength="5"
                                           id="postcode"
                                           name="postcode">
                                    <label for="postcode">Poskod *</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-6">
                                <div class="form-material floating">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="education"
                                            name="education"
                                            onchange="
                                            function f(v) {
                                                console.log(v);
                                                if (v === 'LAIN-LAIN') {
                                                    $('#div-ext-education').show();
                                                } else {
                                                    $('#div-ext-education').hide();
                                                    $('#inp-ext-education').val('');
                                                }
                                            }
                                            f(this.options[this.selectedIndex].value)">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <option value="SPM">SPM</option>
                                        <option value="STPM">STPM</option>
                                        <option value="DIPLOMA">DIPLOMA</option>
                                        <option value="SARJANA MUDA">SARJANA MUDA</option>
                                        <option value="SARJANA">SARJANA</option>
                                        <option value="DOKTOR FALSAFAH">DOKTOR FALSAFAH</option>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                    </select>
                                    <label for="education">Taraf Pendidikan </label>
                                </div>
                            </div>
                            <div id="div-ext-education" class="col-md-8" style="display:none">
                                <div class="form-material floating">
                                    <label for="inp-ext-education"></label>
                                    <input type="text" name="education" id="inp-ext-education" class="form-control"
                                           placeholder="Sila nyatakan taraf pendidikan anda">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="occupation"
                                            name="occupation"
                                            onchange="
                                            function f(v) {
                                                console.log(v);
                                                if (v === 'LAIN-LAIN') {
                                                    $('#div-ext-occupation').show();
                                                } else {
                                                    $('#div-ext-occupation').hide();
                                                    $('#inp-ext-occupation').val('');
                                                }
                                            }
                                            f(this.options[this.selectedIndex].value)">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <?php
                                        global $commonService;
                                        $occupation = $commonService->getOccupation();
                                        foreach ($occupation as $i) {
                                            echo '<option value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                        }
                                        ?>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                    </select>
                                    <label for="occupation">Pekerjaan *</label>
                                </div>
                            </div>
                            <div id="div-ext-occupation" class="col-md-8" style="display:none">
                                <div class="form-material floating">
                                    <label for="inp-ext-occupation"></label>
                                    <input type="text" name="occupation" id="inp-ext-occupation" class="form-control"
                                           placeholder="Sila nyatakan pekerjaan anda">
                                </div>
                            </div>
                        </div>                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material floating">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="employer-address"
                                           name="employer-address">
                                    <label for="employer-address">Alamat majikan </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="race"
                                            name="race"
                                            onchange="
                                            function f(v) {
                                                console.log(v);
                                                if (v === 'LAIN-LAIN') {
                                                    $('#div-ext-race').show();
                                                } else {
                                                    $('#div-ext-race').hide();
                                                    $('#inp-ext-race').val('');
                                                }
                                            }
                                            f(this.options[this.selectedIndex].value)">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <?php
                                        global $commonService;
                                        $races = $commonService->getRaces();
                                        foreach ($races as $i) {
                                            echo '<option value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                        }
                                        ?>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                    </select>
                                    <label for="race">Bangsa *</label>
                                </div>
                            </div>
                            <div id="div-ext-race" class="col-md-8" style="display:none">
                                <div class="form-material floating">
                                    <label for="inp-ext-race"></label>
                                    <input type="text" name="inp-ext-race" id="inp-ext-race" class="form-control"
                                           placeholder="Sila nyatakan bangsa anda">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="religion"
                                            name="religion"
                                            onchange="
                                            function f(v) {
                                                console.log(v);
                                                if (v === 'LAIN-LAIN') {
                                                    $('#div-ext-religion').show();
                                                } else {
                                                    $('#div-ext-religion').hide();
                                                    $('#inp-ext-religion').val('');
                                                }
                                            }
                                            f(this.options[this.selectedIndex].value)">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <?php
                                        global $commonService;
                                        $religions = $commonService->getReligions();
                                        foreach ($religions as $i) {
                                            echo '<option value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                        }
                                        ?>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                    </select>
                                    <label for="religion">Agama *</label>
                                </div>
                            </div>
                            <div id="div-ext-religion" class="col-md-8" style="display:none">
                                <div class="form-material floating">
                                    <label for="inp-ext-religion"></label>
                                    <input type="text" name="inp-ext-religion" id="inp-ext-religion"
                                           class="form-control"
                                           placeholder="Sila nyatakan agama anda">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="gender"
                                            name="gender">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <option value="LELAKI">LELAKI</option>
                                        <option value="PEREMPUAN">PEREMPUAN</option>
                                    </select>
                                    <label for="gender">Jantina *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <div class="custom-controls-stacked">

                                    <?php
                                    $interests = array('Media & Komunikasi', 'Kesenian & Kebudayaan',
                                        'Sukan & Rekreasi', 'Khidmat masyarakat & Pembangunan komuniti',
                                        'Pembangunan keluarga', 'Ekonomi & Kewangan',
                                        'Pendidikan', 'Siasah & Keadilan sosial',
                                        'Perundangan', 'Perniagaan', 'Lain-lain');

                                    foreach ($interests as $key => $val) {
                                        echo "<label class=\"custom-control custom-checkbox\">
                                                <input type=\"checkbox\" class=\"custom-control-input\"
                                                       name=\"interest[]\" value=\"$val\">
                                                <span class=\"custom-control-indicator\"></span>
                                                <span class=\"custom-control-description\">$val</span>
                                            </label>";
                                    }

                                    ?>
                                </div>
                                <label for="interest">Sila pilih minat anda *</label>
                            </div>
                        </div>
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Jenis Keahlian</h3>
                            </div>
                            <div class="block-content">
                                <div class="form-group row">
                                    <div class="col-xl-6">
                                        <p><strong>Jenis Keahlian</strong>.</p>
                                    </div>
                                    <div class="col-xl-6">
                                        <p><label class="css-control css-control-primary css-radio">
                                                <input type="radio" class="form-control css-control-input"
                                                       name="membershipType"
                                                       value="0">
                                                <span class="css-control-indicator"></span> Asas
                                            </label>
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" class="form-control css-control-input"
                                                       name="membershipType"
                                                       value="1">
                                                <span class="css-control-indicator"></span> Penuh
                                            </label>
										</p>
                                    </div>
                                </div>
                            </div>
                             <div class="block-content">
                                <div class="form-group row">
                                    <div class="col-xl-6">
                                        <p><strong>Status Keahlian</strong>.</p>
                                    </div>
                                    <div class="col-xl-6">
                                        <p><label class="css-control css-control-primary css-radio">
                                                <input type="radio" class="form-control css-control-input"
                                                       name="activeStatus"
                                                       value="1">
                                                <span class="css-control-indicator"></span> Aktif
                                            </label>
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" class="form-control css-control-input"
                                                       name="activeStatus"
                                                       value="0">
                                                <span class="css-control-indicator"></span> Tidak Aktif
                                            </label>
										</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-alt-primary">
                                <i class="fa fa-save mr-5"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Sign Up Form -->
        </div>
    </div>
    <!-- END Page Content -->




<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

<!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>
<?php $cb->get_js('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>
<?php $cb->get_js('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js'); ?>
<?php $cb->get_js('js/plugins/select2/select2.full.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-tags-input/jquery.tagsinput.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-auto-complete/jquery.auto-complete.min.js'); ?>
<?php $cb->get_js('js/plugins/masked-inputs/jquery.maskedinput.min.js'); ?>
<?php $cb->get_js('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js'); ?>
<?php $cb->get_js('js/plugins/dropzonejs/min/dropzone.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-validation/additional-methods.min.js'); ?>


<?php $cb->get_js('js/plugins/moment/moment.min.js'); ?>


<!-- Page JS Code -->
<script>
        $('#nric').keyup(function () {
                if (this.value.length === 12) {
                    compact = this.value.substring(0, 6);
                    let age = moment().diff(moment(compact, 'YYMMDD').toDate(), 'years');
                    $('#age').val(age);
                } else {
                    $('#age').val('');
                }
            }
        );

        $('#nric').keyup();
    </script>

<?php $cb->get_js('js/pages/op_auth_signup.js'); ?>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>
<?php $cb->get_js('js/pages/be_forms_validation.js'); ?>
<?php require 'inc/_global/views/footer_end.php'; ?>