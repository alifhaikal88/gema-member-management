<?php
require dirname(__FILE__) . '/../admin/inc/_global/config.php';
require dirname(__FILE__) . '/../admin/inc/backend/config.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_start.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_end.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../services/CommonService.php';
global $commonService;
$races = $commonService->getRaces();
?>

    <div class="content">
        <h2 class="content-heading">Bangsa</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Senarai Bangsa</h3>
                <input type="button" class="btn btn-primary min-width-125" value="New"
                       onclick="window.location.href='RaceNew.php'">
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 50px">#</th>
                        <th>Nama</th>
                        <th class="text-center" style="width: 15%;">Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($races as $i => $v) {
                        echo '<tr>
                                <td class="text-center">' . ++$i . '</td>
                                <td class="font-w600">' . $v['name'] . '</td>
                                <td class="text-center">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Perinci">
                                    <a href="RaceDetail.php?id=' . $v["id"] . '"><i class="fa fa-edit"></i></a>
                                </button>
                                </td>
                            </tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full -->

    </div>

    <!-- END Page Container -->
<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>

<?php require 'inc/_global/views/footer_end.php'; ?>