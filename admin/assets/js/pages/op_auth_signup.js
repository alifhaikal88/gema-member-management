/*
 *  Document   : op_auth_signup.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Sign Up Page
 */

var OpAuthSignUp = function () {
    // Init Sign Up Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationSignUp = function () {
        jQuery('.js-validation-signup').validate({
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function (error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function (e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function (e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            submitHandler: function (form) {
                // target_popup(form);
                form.submit();
                checkStatus();
            },
            rules: {
                "interest[]": {
                    required: true,
                    minlength: 1
                },
                'signup-username': {
                    required: true,
                    minlength: 3
                },
                'nomember': {
                    required: true,
                },
                'registrationDate': {
                    required: true,
                },
                'expiredDate': {
                    required: true,
                },
                'fullname': {
                    required: true,
                    minlength: 3
                },
                'nric': {
                    required: true,
                    minlength: 12,
                    maxlength: 12,                    
                },
                'address': {
                    required: true,
                },
                'postcode': {
                    required: true,
                },
                'city': {
                    required: true,
                },
                'state': {
                    required: true,
                },
                'employer-addressx': {
                    required: true,
                },
                'mobile': {
                    required: true,
                },
                'email': {
                    required: true,
                    email: true
                },
                'occupation': {
                    required: true
                },
                'branch': {
                    required: true
                },
                'education': {
                    required: false
                },
                'race': {
                    required: true
                },
                'inp-ext-race': {
                    required: function () {
                        return $('#race').val() === 'LAIN-LAIN';
                    }
                },
                'religion': {
                    required: true
                },
                'inp-ext-religion': {
                    required: function () {
                        return $('#religion').val() === 'LAIN-LAIN';
                    }
                },
                'gender': {
                    required: true
                },
                'interest': {
                    required: true
                },
                'membershipType': {
                    required: true
                }
            },
            messages: {
                "interest[]": {
                    required: 'Sila pilih sekurang-kurangnya satu minat',
                    minlength: 'Sila pilih sekurang-kurangnya satu minat'
                },
                'signup-username': {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'fullname': {
                    required: 'Sila masukkan nama penuh',
                },
                'nric': {
                    required: 'Sila masukkan no kad pengenalan',
                    minlength: 'Sila masukkan 12 digit no. kad pengenalan!',
                    maxlength: 'Sila masukkan 12 digit no. kad pengenalan!',
                    remote: 'No IC ini telah didaftarkan'
                },
                'age': {
                    remote: 'XXX'
                },
                'nomember': {
                    required: 'Sila masukkan no ahli',
                },
                'registrationDate': {
                    required: 'Sila masukkan tarikh daftar',
                },
                'expiredDate': {
                    required: 'Sila masukkan tarikh tamat keahlian',
                },
                'address': {
                    required: 'Sila masukkan alamat',
                },
                'postcode': {
                    required: 'Sila masukkan poskod',
                },
                'city': {
                    required: 'Sila masukkan bandar',
                },
                'state': {
                    required: 'Sila masukkan negeri',
                },
                'employer-addressx': {
                    required: 'Sila masukkan alamat majikan anda',
                },
                'mobile': {
                    required: 'Sila masukkan no telefon bimbit',
                },
                'race': {
                    required: 'Sila pilih bangsa'
                },
                'inp-ext-race': {
                    required: 'Sila nyatakan bangsa anda'
                },
                'religion': {
                    required: 'Sila pilih agama'
                },
                'inp-ext-religion': {
                    required: 'Sila nyatakan agama anda'
                },
                'gender': {
                    required: 'Sila pilih jantina'
                },
                'occupation': {
                    required: 'Sila masukkan pekerjaan'
                },
                'branch': {
                    required: 'Sila pilih cawangan GEMA'
                },
                
                'membershipType': {
                    required: 'Sila pilih jenis keahlian',
                },
                'email': 'Sila masukkan alamat emel yang sah',
                'signup-password': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long'
                },
                'signup-password-confirm': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above'
                },
                'signup-terms': 'You must agree to the service terms!'
            }
        });

        function checkStatus() {
            var submitBtn = $("button[type=submit]");
            submitBtn.prop('disabled', 'disabled');
            submitBtn.html('<i class="fa fa-gears mr-5"></i> Sedang diproses...');
        }
    };

    return {
        init: function () {
            // Init SignUp Form Validation
            initValidationSignUp();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    OpAuthSignUp.init();
});