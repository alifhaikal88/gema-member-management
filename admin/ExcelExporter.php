<?php

ob_start();
session_start();

require dirname(__FILE__) . '/../vendor/PHPExcel/Classes/PHPExcel.php';

define("DB_HOST", "mevyekasu.com.my");
define("DB_USER", "mevyekas_gema");
define("DB_PASS", "kompangdipalu");
define("SQL_DB", "mevyekas_gema");
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, SQL_DB);

// Check connection
if (mysqli_connect_errno()) {
    echo "Unable to connect to database: " . mysqli_connect_error();
}

$sql = 'select upper(registration_no) registration_no,
               approve_date registration_date,
               expired_at expired_date,
               membership_type,
               name,
               age,
               gender,
               dob,
               nric,
               phone,
               mobile,
               email,
               concat_ws(\' \', upper(address), postcode, upper(city), upper(state)) address,
               upper(occupation) occupation,
               upper(education) education,
               upper(race) race,
               upper(religion) religion,
               upper(interest) interest
               from g_registration where paid = 1 order by name';

$result = $conn->query($sql);
$conn->close();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'BIL')
    ->setCellValue('B1', 'NO PENDAFTARAN')
    ->setCellValue('C1', 'TKH PENDAFTARAN')
    ->setCellValue('D1', 'TKH TAMAT')
    ->setCellValue('E1', 'NAME')
    ->setCellValue('F1', 'UMUR')
    ->setCellValue('G1', 'JANTINA')
    ->setCellValue('H1', 'TKH LAHIR')
    ->setCellValue('I1', 'NRIC')
    ->setCellValue('J1', 'NO TELEFON')
    ->setCellValue('K1', 'NO HP')
    ->setCellValue('L1', 'EMEL')
    ->setCellValue('M1', 'ALAMAT')
    ->setCellValue('N1', 'PENDIDIKAN')
    ->setCellValue('O1', 'BANGSA')
    ->setCellValue('P1', 'AGAMA')
    ->setCellValue('Q1', 'MINAT')
    ->setCellValue('R1', 'PEKERJAAN')
    ->setCellValue('S1', 'JENIS KEAHLIAN');

// Repeat data from database
$i = 2; //default starting
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $i - 1);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row["registration_no"]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row["registration_date"]);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row["expired_date"]);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row["name"]);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row["age"]);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row["gender"]);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row["dob"]);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row["nric"],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row["phone"]);
        $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row["mobile"]);
        $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row["email"]);
        $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row["address"]);
        $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row["education"]);
        $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $row["race"]);
        $objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row["religion"]);
        $objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row["interest"]);
        $objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $row["occupation"]);
        if ($row["membership_type"] == '0') {
            $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, "ASAS");
        } elseif ($row["membership_type"] == '1') {
            $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, "PENUH");
        } else {
            $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, "ERROR");
        }
        $i++;
    }
}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('SENARAI KEAHLIAN GEMA');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client�s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Senarai_Ahli_GEMA.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;

?>