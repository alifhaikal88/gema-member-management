<?php
require dirname(__FILE__) . '/../admin/inc/_global/config.php';
require dirname(__FILE__) . '/../admin/inc/backend/config.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_start.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_end.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../admin/../DBConn.php';
require dirname(__FILE__) . '/../admin/../services/CommonService.php';

global $commonService;

if (isset($_GET) && isset($_GET['id'])) {
    $staff = $commonService->getStaffById($_GET['id']);
}

if (isset($_POST)) {
    if (isset($_POST['submit'])) {
        $newStaff = [
            'id' => $_GET['id'],
            'staffName' => strtoupper($_POST['staffName']),
            'staffIc' => $_POST['staffIc'],
            'staffState' => $_POST['staffState'],
            'staffEmail' => $_POST['staffEmail'],

        ];

        $commonService->updateStaff($newStaff);
        echo "<script>window.location.href = 'StaffList.php'</script>";

    } else if (isset($_POST['delete'])) {
        $commonService->deleteStaff(array('id' => $_GET['id']));
        echo "<script>window.location.href = 'StaffList.php'</script>";
    }
}

?>


    <div class="content">
        <h2 class="content-heading">Staf</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Maklumat Staf</h3>
            </div>
            <div class="block-content block-content-full">
                <form class="settings-form" action="#" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffName"
                                       name="staffName"
                                       value="<? echo $staff['staff_name'] ?>">
                                <label for="name">Nama</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffIc"
                                       name="staffIc"
                                       value="<? echo $staff['staff_ic'] ?>">
                                <label for="name">No. IC</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffState"
                                       name="staffState"
                                       value="<? echo $staff['staff_state'] ?>">
                                <label for="name">Cawangan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffEmail"
                                       name="staffEmail"
                                       value="<? echo $staff['staff_email'] ?>">
                                <label for="name">Emel</label>
                            </div>
                        </div>
                    </div>



                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" name="submit" class="btn btn-alt-info">
                                <i class="fa fa-save mr-5"></i> Save
                            </button>
                            <button type="submit" name="delete" class="btn btn-alt-warning">
                                <i class="fa fa-remove mr-5"></i> Delete
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Dynamic Table Full -->

    </div>

    <!-- END Page Container -->
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/page_end.php'; ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/pages/race-form-validation.js'); ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_end.php'; ?>