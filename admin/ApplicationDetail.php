<?php
require 'inc/_global/config.php';
require 'inc/backend/config.php';
require 'inc/_global/views/head_start.php';

$cb->get_css('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); 
$cb->get_css('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'); 
$cb->get_css('js/plugins/select2/select2.min.css'); 
$cb->get_css('js/plugins/select2/select2-bootstrap.min.css'); 
$cb->get_css('js/plugins/jquery-tags-input/jquery.tagsinput.min.css'); 

$cb->get_css('js/plugins/select2/select2-bootstrap.min.css');
$cb->get_css('js/plugins/select2/select2.min.css');
require 'inc/_global/views/head_end.php';
require 'inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../services/RegistrationService.php';

global $registrationService;

if (isset($_GET) && isset($_GET['id'])) {
    $a = $registrationService->getRegistrationById($_GET['id']);
    $id = $_GET['id'];
    $_SESSION['id'] = $id;
}

?>

<!-- Page Content -->
<div class="content">
    <!-- Bootstrap Forms Validation -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Maklumat Pemohon</h3>
        </div>
        <div class="block-content">
            <div class="row justify-content-center">
                <div class="col-md-6 col-xl-4">
                    <a class="block block-link-shadow text-right" href="javascript:void(0)">
                        <div class="block-content block-content-full clearfix">
                            <div class="float-left mt-10">
                                <i class="si si-clock fa-3x text-body-bg-dark"></i>
                            </div>
                            <div class="font-size-h3 font-w600 text-primary"><? echo date_format(new  DateTime($a['c_ts']), 'd/m/y H:i:s') ?></div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Tarikh Pendaftaran</div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-xl-3">
                    <a class="block block-link-shadow text-right" href="javascript:void(0)">
                        <div class="block-content block-content-full clearfix">
                            <div class="float-left mt-10">
                                <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                            </div>
                            <div class="font-size-h3 font-w600 text-primary"><? echo $a['membership_type'] == 0 ? 'BIASA' : 'PENUH' ?></div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Jenis Keahlian</div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-xl-3">
                    <a class="block block-link-shadow text-right" href="javascript:void(0)">
                        <div class="block-content block-content-full clearfix">
                            <div class="float-left mt-10">
                                <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                            </div>
                            <div class="font-size-h3 font-w600 <? echo $a['paid'] == 0 ? 'text-danger' : 'text-primary' ?>"><? echo $a['paid'] == 0 ? 'TIDAK' : 'YA' ?></div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Status Bayaran</div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <form id="sign-up-form" class="js-validation-signup" action=""
                          method="post"
                          enctype="application/x-www-form-urlencoded">
                        <div class="form-group row">
                        </div>
                        <legend class="h5 font-w700 mt-50 mb-10">Maklumat Pemohon</legend>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="no-member"
                                           name="no-member"
                                           value="<? echo $a['registration_no'] ?>">
                                    <label for="no-member">No. Ahli *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    <input type="text" class="js-datepicker form-control" id="registration-date" name="registration-date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder=""
                                    value="<? echo $a['approve_date'] ?>">
                                    <label for="registration-date">Tarikh Daftar *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    <input type="text" class="js-datepicker form-control" id="expired-date" name="expired-date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder=""
                                    value="<? echo $a['expired_at'] ?>">
                                    <label for="expired-date">Tarikh Tamat Tempoh *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="fullname"
                                           name="fullname"
                                           value="<? echo $a['name'] ?>">
                                    <label for="fullname">Nama Penuh *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-8">
                                <div class="form-material">
                                    <input type="text" class="form-control"
                                           onkeydown="return ( event.ctrlKey || event.altKey ||
                                               (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) ||
                                                (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) ||
                                                 (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )"
                                           id="nric" name="nric" minlength="12" maxlength="12"
                                           value="<? echo $a['nric'] ?>"
                                        <? if ($_GET['nric']) echo 'readonly'; else echo ''; ?>>
                                    <label for="nric">No. Kad Pengenalan *</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-material open">
                                    <input type="text" class="form-control"
                                           id="age"
                                           name="age"
                                           value="<? echo $a['age'] ?>"
                                           readonly>
                                    <label for="age">Umur</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="branch" name="branch"
                                        style="width: 100%;" data-placeholder="Sila Pilih">
                                    <option></option>
                                    <?php
                                    global $commonService;
                                    $religions = $commonService->getBranches();
                                    foreach ($religions as $i) {
                                        $selected = '';
                                        if ($i['name'] === $a['branch']) $selected = 'selected';

                                        echo '<option ' . $selected . ' value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                                <label for="branch">Cawangan Gema *</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material">
                                    <input type="email" class="form-control"
                                           id="email"
                                           name="email"
                                           value="<? echo $a['email'] ?>">
                                    <label for="email">Email *</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-material">
                                    <input type="text" class="form-control"
                                           id="phone"
                                           name="phone"
                                           value="<? echo $a['phone'] ?>">
                                    <label for="phone">No Telefon</label>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <div class="form-material">
                                    <input type="text" class="form-control"
                                           id="mobile"
                                           name="mobile"
                                           value="<? echo $a['mobile'] ?>">
                                    <label for="mobile">No Hp *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="address"
                                           name="address"
                                           value="<? echo $a['address'] ?>">
                                    <label for="address">Alamat surat menyurat *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="city"
                                           name="city"
                                           value="<? echo $a['city'] ?>">
                                    <label for="city">Bandar *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            data-placeholder="Sila Pilih"
                                            id="state"
                                            name="state">
                                        <option></option>

                                        <?php
                                        $states = array('PERLIS', 'KEDAH', 'PULAU PINANG', 'PERAK',
                                            'SELANGOR', 'KUALA LUMPUR', 'NEGERI SEMBILAN', 'MELAKA',
                                            'JOHOR', 'PAHANG', 'TERENGGANU', 'KELANTAN', 'SABAH', 'SARAWAK');

                                        foreach ($states as $i) {
                                            $selected = '';
                                            if ($i === $a['state']) $selected = 'selected';
                                            echo '<option ' . $selected . ' value="' . $i . '">' . $i . '</option>';
                                        }
                                        ?>

                                    </select>
                                    <label for="state">Negeri *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    <input type="text"
                                           onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )"
                                           class="form-control"
                                           maxlength="5"
                                           id="postcode"
                                           name="postcode"
                                           value="<? echo $a['postcode'] ?>">
                                    <label for="postcode">Poskod *</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                        <div class="col-6">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="education"
                                           name="education"
                                           value="<? echo $a['education'] ?>">
                                    <label for="education">Taraf Pendidikan *</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="occupation"
                                           name="occupation"
                                           value="<? echo $a['occupation'] ?>">
                                    <label for="occupation">Pekerjaan *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material">
                                    <input type="text" style="text-transform:uppercase" class="form-control"
                                           id="employer-address"
                                           name="employer-address"
                                           value="<? echo $a['employer_address'] ?>">
                                    <label for="employer-address">Alamat majikan *</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="form-material">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            id="race"
                                            name="race"
                                            onchange="
                                                function f(v) {
                                                    console.log(v);
                                                    if (v === 'LAIN-LAIN') {
                                                        $('#div-ext-race').show();
                                                    } else {
                                                        $('#div-ext-race').hide();
                                                        $('#inp-ext-race').val('');
                                                    }
                                                }
                                                f(this.options[this.selectedIndex].value)">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <?php
                                        global $commonService;
                                        $races = $commonService->getRaces();
                                        foreach ($races as $i) {
                                            $selected = '';
                                            if ($i['name'] === $a['race']) $selected = 'selected';
                                            echo '<option ' . $selected . ' value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                        }
                                        ?>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                    </select>
                                    <label for="race">Bangsa *</label>
                                </div>
                            </div>
                            <div id="div-ext-race" class="col-md-8" style="display:none">
                                <div class="form-material">
                                    <label for="inp-ext-race"></label>
                                    <input type="text" class="form-control"
                                           id="inp-ext-race"
                                           name="inp-ext-race"
                                           placeholder="Sila nyatakan bangsa anda"
                                           value="<? echo $a['ext_race'] ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            data-placeholder="Sila Pilih"
                                            id="religion"
                                            name="religion"
                                            onchange="
                                                function f(v) {
                                                    console.log(v);
                                                    if (v === 'LAIN-LAIN') {
                                                        $('#div-ext-religion').show();
                                                    } else {
                                                        $('#div-ext-religion').hide();
                                                        $('#inp-ext-religion').val('');
                                                    }
                                                }
                                                f(this.options[this.selectedIndex].value)">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <?php
                                        global $commonService;
                                        $religions = $commonService->getReligions();
                                        foreach ($religions as $i) {
                                            $selected = '';
                                            if ($i['name'] === $a['religion']) $selected = 'selected';
                                            echo '<option ' . $selected . ' value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                        }
                                        ?>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                    </select>
                                    <label for="religion">Agama *</label>
                                </div>
                            </div>
                            <div id="div-ext-religion" class="col-md-8" style="display:none">
                                <div class="form-material">
                                    <label for="inp-ext-religion"></label>
                                    <input type="text"
                                           class="form-control"
                                           id="inp-ext-religion"
                                           name="inp-ext-religion"
                                           placeholder="Sila nyatakan agama anda"
                                        <? echo $a['ext_religion'] ?>>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            data-placeholder="Sila Pilih"
                                            id="gender"
                                            name="gender">
                                        <option></option>
                                        <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        <?
                                        $genders = ['LELAKI', 'PEREMPUAN'];
                                        foreach ($genders as $i) {
                                            $selected = '';
                                            if ($i === $a['gender']) $selected = 'selected';
                                            echo '<option ' . $selected . ' value="' . $i . '">' . $i . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <label for="gender">Jantina *</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <div class="custom-controls-stacked">

                                    <?php
                                    $interests = $commonService->getInterest();
                                    foreach ($interests as $key => $val) {
                                        $v = $val['name'];
                                        $checked = '';
                                        if (array_key_exists($v, array_flip(json_decode($a['interest'])))) $checked = 'checked';
                                        echo "<label class=\"custom-control custom-checkbox\">
                                                    <input type=\"checkbox\" class=\"custom-control-input\"
                                                           name=\"interest[]\" value=\"$v\" $checked>
                                                    <span class=\"custom-control-indicator\"></span>
                                                    <span class=\"custom-control-description\">$v</span>
                                                </label>";
                                    }
                                    ?>
                                </div>
                                <label for="interest">Minat</label>
                            </div>
                        </div>
                        <div class="block">                            
                            <div class="block-content">
                                <div class="form-group row">
                                    <div class="col-xl-6">
                                        <p><strong>Jenis Keahlian</strong>.</p>
                                    </div>
                                    <div class="col-xl-6">
                                        <p><label class="css-control css-control-primary css-radio">
                                                <input type="radio" class="form-control css-control-input"
                                                       name="membershipType"
                                                       value="0" <?php echo ($a['membership_type']=='0')?'checked':'' ?>>
                                                <span class="css-control-indicator"></span> Asas
                                            </label>
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" class="form-control css-control-input"
                                                       name="membershipType"
                                                       value="1" <?php echo ($a['membership_type']=='1')?'checked':'' ?>>
                                                <span class="css-control-indicator"></span> Penuh
                                            </label>
										</p>
                                    </div>
                                </div>
                            </div>                             
                        </div>

                        <?php
                        if ($_GET['mode'] != 'view') {
                            echo '<button name="reconfirmPayment" id="reconfirmPayment" type="submit" formaction="ReconfirmPayment.php" class="btn btn-primary">Terima</button>
                                      <button name="reject" id="reject" type="submit" formaction="RejectApplication.php" class="btn btn-danger">Tolak</button>';
                        } else {
                            echo '<button name="update" id="update" type="submit" formaction="UpdateApplication.php" class="btn btn-primary">Kemaskini</button>';                        }
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap Forms Validation -->
</div>
<!-- END Page Content -->

<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

<!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/select2/select2.full.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-validation/additional-methods.min.js'); ?>


<!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>
<?php $cb->get_js('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>
<?php $cb->get_js('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-tags-input/jquery.tagsinput.min.js'); ?>
<?php $cb->get_js('js/plugins/jquery-auto-complete/jquery.auto-complete.min.js'); ?>
<?php $cb->get_js('js/plugins/masked-inputs/jquery.maskedinput.min.js'); ?>
<?php $cb->get_js('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js'); ?>
<?php $cb->get_js('js/plugins/dropzonejs/min/dropzone.min.js'); ?>
<?php $cb->get_js('js/plugins/moment/moment.min.js'); ?>

<!-- Page JS Code -->
<script>
    jQuery(function () {
        // Init page helpers (Select2 plugin)
        Codebase.helpers('select2');
    });
</script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>
<?php $cb->get_js('js/pages/be_forms_validation.js'); ?>
<?php require 'inc/_global/views/footer_end.php'; ?>