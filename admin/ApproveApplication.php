<?php

session_start();

require dirname(__FILE__) . '/../services/RegistrationService.php';

global $registrationService;

if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: ../Login.php');
	}

$registrationService->approve($_SESSION['id']);

echo "<script>alert('Permohonan berjaya diterima');";
print "window.location='ApplicationList.php'";
print "</script> ";

?>