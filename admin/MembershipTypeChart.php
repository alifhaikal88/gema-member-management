<?php
require 'inc/_global/config.php';
require 'inc/backend/config.php';
require 'inc/_global/views/head_start.php';
require 'inc/_global/views/head_end.php';
require 'inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../services/DashboardService.php';

global $dashboardService;

$countMembershipType = $dashboardService->getMembershipType();
?>

<div class="content">

    <div class="col-xl-12">
    <!-- Easy Pie Chart (.js-pie-chart class is initialized in Codebase() -> uiHelperEasyPieChart()) -->
    <!-- For more info and examples you can check out http://rendro.github.io/easy-pie-chart/ -->
    <!-- Randomize Values Buttons functionality initialized in js/pages/be_comp_charts.js -->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Membership Type', 'Number'],
          
          <?php
           foreach ($countMembershipType as $i => $v) {
              if ($v["membership_type"] == '1'){
                $member = "Penuh";
              }
              else if ($v["membership_type"] == '0'){
                $member = "Biasa";
              }
              else {
                $member = "Null";
              }
              echo "['".$member."', ".$v["number"]."],";
           }
          ?>
          
        ]);

        var options = {
          title: 'Jenis Keahlian',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  
  <body>
    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
  </body>
</div>
</div>

