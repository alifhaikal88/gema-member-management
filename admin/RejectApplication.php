<?php

session_start();

require dirname(__FILE__) . '/../services/RegistrationService.php';

global $registrationService;

if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: ../Login.php');
	}

$registrationService->reject($_SESSION['id']);

echo "<script>alert('Permohonan berjaya ditolak');";
print "window.location='ApplicationList.php'";
print "</script> ";

?>