<?php
require 'inc/_global/config.php';
require 'inc/backend/config.php';
require 'inc/_global/views/head_start.php';
require 'inc/_global/views/head_end.php';
require 'inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../services/DashboardService.php';

global $dashboardService;

$countBranch = $dashboardService->getBranchChart();
?>

<div class="content">

    <div class="col-xl-12">
    <!-- Easy Pie Chart (.js-pie-chart class is initialized in Codebase() -> uiHelperEasyPieChart()) -->
    <!-- For more info and examples you can check out http://rendro.github.io/easy-pie-chart/ -->
    <!-- Randomize Values Buttons functionality initialized in js/pages/be_comp_charts.js -->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Branch', 'Number'],
          <?php
           foreach ($countBranch as $i => $v) {
              echo "['".$v["branch"]."', ".$v["number"]."],";
           }
          ?>
          
        ]);

        var options = {
          title: 'CAWANGAN',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  
  <body>
    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
  </body>
</div>
</div>

