<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/backend/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>

    <!-- Page JS Plugins CSS -->
<?php $cb->get_css('js/plugins/datatables/dataTables.bootstrap4.min.css'); ?>

<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Senarai Ahli Ditolak</h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th>Nama</th>
                        <th class="d-none d-sm-table-cell">Cawangan Gema</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">No. MyKad</th>
                        <th class="text-center" style="width: 15%;">Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sql = "select * from `g_registration` where status='REJECTED' order by `id`";
                    $result = mysqli_query($conn, $sql) or die ('Data Permohonan cannot be reach. ' . mysqli_error($conn));
                    $i = 1;
                    while ($record = mysqli_fetch_array($result)) {
                        $id = $record["id"]; ?>
                        <tr>
                            <td class="text-center"><? echo $i; ?></td>
                            <td class="font-w600"><? echo $record["name"]; ?></td>
                            <td class="d-none d-sm-table-cell"><? echo $record["branch"]; ?></td>
                            <td class="d-none d-sm-table-cell"><? echo $record["nric"]; ?></td>
                            <td class="text-center">

                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Perinci">

                                    <a href="ApplicationDetail.php?id=<? echo $record["id"]; ?>"><i
                                                class="fa fa-user"></i></a>
                                </button>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full -->

    </div>
    <!-- END Page Content -->

<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/datatables/jquery.dataTables.min.js'); ?>
<?php $cb->get_js('js/plugins/datatables/dataTables.bootstrap4.min.js'); ?>

    <!-- Page JS Code -->
<?php $cb->get_js('js/pages/be_tables_datatables.js'); ?>

<?php require 'inc/_global/views/footer_end.php'; ?>