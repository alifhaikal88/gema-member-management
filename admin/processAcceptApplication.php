<?php
session_start();
$usertype = $_SESSION["user_type"];
if ($usertype != '1') {
    header('Location:../');
    exit();
}

include '../connection/data2.php';
include '../DBConn.php';

$username = $_SESSION['username'];
$userIC = $_SESSION['user_ic'];
$staff_IC = $_SESSION['staff_ic'];

$id = $_SESSION['id'];
$status = "APPROVED";


$query = "UPDATE g_registration SET status='$status' WHERE id='$id'";
$result = mysqli_query($conn, $query) or die ('Data permohonan cannot be reached. ' . mysqli_error($conn));


$where = ['id' => $id];
$stmt = $pdo->prepare('select * from g_registration where id = :id');
$stmt->execute($where);
$registrationResult = $stmt->fetch();

serializeToMembership($pdo, $registrationResult);

function serializeToMembership($pdo, $registrationResult)
{
    $expiredAt = null;
    if ($registrationResult['membership_type'] == '0') {
        $expiredAt = date('Y-m-d', strtotime('+1 year'));
    } else {
        $age = $registrationResult['age'];
        $diff = 36 - $age;
        $expiredAt = date('Y-m-d', strtotime('+' . $diff . ' year'));
    }


    $params = [
        'id' => $registrationResult['id'],
        'expiredAt' => $expiredAt
    ];

    $sql = 'insert into g_membership (name, age, nric, branch, email, phone, mobile, address, city, state, postcode, education,
                          occupation, race, religion, gender, interest, membership_type, expired_at, renew_at, active,
                          c_ts, m_ts)
  (select
     name,
     age,
     nric,
     branch,
     email,
     phone,
     mobile,
     address,
     city,
     state,
     postcode,
     education,
     occupation,
     race,
     religion,
     gender,
     interest,
     membership_type,
     :expiredAt,
     null,
     true,
     now(),
     null
   from g_registration
   where id = :id)';

    $pdo->prepare($sql)->execute($params);
}


echo "<script>alert('Permohonan berjaya diterima');";
print "window.location='new_application_list.php'";
print "</script> ";

?>