<?php
require dirname(__FILE__) . '/../admin/inc/_global/config.php';
require dirname(__FILE__) . '/../admin/inc/backend/config.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_start.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_end.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../admin/../DBConn.php';
require dirname(__FILE__) . '/../admin/../services/CommonService.php';

global $commonService;

if (isset($_POST) && isset($_POST['submit'])) {

    $newBranch = [
        'name' => strtoupper($_POST['name']),
        'billplzSecretKey' => $_POST['billplzSecretKey'],
        'billplzCollectionId' => $_POST['billplzCollectionId'],
    ];

    $commonService->saveBranch($newBranch);
    echo "<script>window.location.href = 'BranchList.php'</script>";
}

?>


    <div class="content">
        <h2 class="content-heading">Cawangan</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Cawangan Baru</h3>
            </div>
            <div class="block-content block-content-full">
                <form class="settings-form" action="#" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="name"
                                       name="name"
                                       value=""
                                       placeholder="cth: KUALA LUMPUR">
                                <label for="name">Nama</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control"
                                       id="billplzSecretKey"
                                       name="billplzSecretKey"
                                       value=""
                                       placeholder="cth: ea315173-2e09-418c-98a2-47c7d005f8fe">
                                <label for="name">Billplz Secret Key</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control"
                                       id="billplzCollectionId"
                                       name="billplzCollectionId"
                                       value=""
                                       placeholder="cth: zlzz2upj">
                                <label for="name">Billplz Collection Id</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" name="submit" class="btn btn-alt-info">
                                <i class="fa fa-save mr-5"></i> Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Dynamic Table Full -->

    </div>

    <!-- END Page Container -->
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/page_end.php'; ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/pages/branch-form-validation.js'); ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_end.php'; ?>