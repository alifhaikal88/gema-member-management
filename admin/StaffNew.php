<?php
require dirname(__FILE__) . '/../admin/inc/_global/config.php';
require dirname(__FILE__) . '/../admin/inc/backend/config.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_start.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_end.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../admin/../DBConn.php';
require dirname(__FILE__) . '/../admin/../services/CommonService.php';

global $commonService;

if (isset($_POST) && isset($_POST['submit'])) {

    $newStaff = [
        'staffName' => strtoupper($_POST['staffName']),
        'staffIc' => $_POST['staffIc'],
        'staffState' => $_POST['staffState'],
        'staffEmail' => $_POST['staffEmail'],
        'userName' => $_POST['userName'],
        'password' => $_POST['password'],
    ];

    $commonService->saveStaff($newStaff);
    echo "<script>window.location.href = 'StaffList.php'</script>";
}

?>
    <div class="content">
        <h2 class="content-heading">Staff</h2>
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Staff Baru</h3>
            </div>
            <div class="block-content block-content-full">
                <form class="settings-form" action="#" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffName"
                                       name="staffName"
                                       value=""
                                       placeholder="">
                                <label for="name">Nama</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control"
                                       id="staffIc"
                                       name="staffIc"
                                       value=""
                                       placeholder="">
                                <label for="name">No. IC</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                                <div class="form-material input-group">
                                    <select class="js-select2 form-control"
                                            style="width: 100%;"
                                            data-placeholder="Sila Pilih"
                                            id="staffState"
                                            name="staffState">
                                        <option></option>
                                        <option value="PERLIS">PERLIS</option>
                                        <option value="KEDAH">KEDAH</option>
                                        <option value="PULAU PINANG">PULAU PINANG</option>
                                        <option value="PERAK">PERAK</option>
                                        <option value="SELANGOR">SELANGOR</option>
                                        <option value="KUALA LUMPUR">KUALA LUMPUR</option>
                                        <option value="NEGERI SEMBILAN">NEGERI SEMBILAN</option>
                                        <option value="MELAKA">MELAKA</option>
                                        <option value="JOHOR">JOHOR</option>
                                        <option value="PAHANG">PAHANG</option>
                                        <option value="TERENGGANU">TERENGGANU</option>
                                        <option value="KELANTAN">KELANTAN</option>
                                        <option value="SABAH">SABAH</option>
                                        <option value="SARAWAK">SARAWAK</option>
                                    </select>
                                    <label for="state">Cawangan</label>
                                </div>
                            </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control"
                                       id="staffEmail"
                                       name="staffEmail"
                                       value=""
                                       placeholder="">
                                <label for="name">Emel</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control"
                                       id="userName"
                                       name="userName"
                                       value=""
                                       placeholder="">
                                <label for="name">Nama Pengguna</label>
                            </div>
                        </div>
                    </div>
					<div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="password" class="form-control text-uppercase"
                                       id="password"
                                       name="password"
                                       value=""
                                       placeholder="">
                                <label for="name">Kata Laluan</label>
                            </div>
                        </div>
                    </div>					
					
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" name="submit" class="btn btn-alt-info">
                                <i class="fa fa-save mr-5"></i> Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Dynamic Table Full -->
    </div>

    <!-- END Page Container -->
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/page_end.php'; ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/pages/branch-form-validation.js'); ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_end.php'; ?>