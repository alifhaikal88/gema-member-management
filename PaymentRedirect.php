<?php

echo 'XXXX';

require dirname(__FILE__) . '/services/PaymentService.php';
require dirname(__FILE__) . '/services/RegistrationService.php';

global $registrationService;
global $paymentService;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$membershipType = $_POST['membershipType'];
$branchName = $_POST['branch'];
$bill = $paymentService->createBill($membershipType, $branchName);
$registrationService->saveRegistrationFromFPX($bill['id']);


echo 'BILL URL ' . $bill['url'];

header("Location:" . $bill['url']);
exit;

?>
