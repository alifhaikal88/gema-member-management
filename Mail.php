<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 8/1/2018
 * Time: 1:03 PM
 */
require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    private $to;
    private $toName;
    private $subject;
    private $body;

    private $mail;

    public function __construct($to, $toName, $subject, $body)
    {
        $this->to = $to;
        $this->toName = $toName;
        $this->subject = $subject;
        $this->body = $body;
    }


    public function init()
    {
        $this->mail = new PHPMailer;

        //Tell PHPMailer to use SMTP
        $this->mail->isSMTP();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $this->mail->SMTPDebug = 2;

        //Set the hostname of the mail server
        $this->mail->Host = 'mail.sigma-gema.com';
        // use
        // $mail->Host = gethostbyname('smtp.gmail.com');
        // if your network does not support SMTP over IPv6

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->Port = 465;

        //Set the encryption system to use - ssl (deprecated) or tls
        $this->mail->SMTPSecure = 'ssl';

        //Whether to use SMTP authentication
        $this->mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $this->mail->Username = "no-reply@sigma-gema.com";

        //Password to use for SMTP authentication
        $this->mail->Password = "dondangsayang";

        //Set who the message is to be sent from
        $this->mail->setFrom('no-reply@sigma-gema.com', 'Team GEMA');

        //Set who the message is to be sent to
        $this->mail->addAddress($this->to, $this->toName);

        //Set the subject line
        $this->mail->Subject = $this->subject;

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);
        $this->mail->Body = $this->body;
        $this->mail->AltBody = "";
        $this->mail->isHTML(true);

    }

    public function send()
    {
        $this->init();
        //send the message, check for errors
        if (!$this->mail->send()) {
            echo "Mailer Error: " . $this->mail->ErrorInfo;
        } else {
            echo "Message sent!";
            //Section 2: IMAP
            //Uncomment these to save your message in the 'Sent Mail' folder.
            #if (save_mail($mail)) {
            #    echo "Message saved!";
            #}
        }
    }

    /**
     * @param mixed $to
     */
    public function setTo($to): void
    {
        $this->to = $to;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    public function renderMailTemplate($tplName, $args)
    {
        $tpl = file_get_contents($tplName);
        $result = preg_replace_callback(
            '~[$]\w{3,20}[$]~',
            function ($matches) use ($args) {
                foreach ($matches as $m) {
                    foreach (array_keys($args) as $a) {
                        if ($a === $m) {
                            echo "ok";
                            return str_replace($a, $args[$a], $m);
                        }
                    }
                }
                return null;
            },
            $tpl);
//        echo $result;

        $this->body = $result;
    }

}