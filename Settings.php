<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>
<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>
<?php require 'DBConn.php'; ?>
<?php
$stmt = $pdo->query("select * from g_configuration");
$fObj = $stmt->fetchAll();

foreach ($fObj as $k => $v) {
    if ($fObj[$k]['k'] === 'membershipNormalAmount')
        $membershipNormalAmount = $fObj[$k]['v'];
    else if ($fObj[$k]['k'] === 'membershipFullAmount')
        $membershipFullAmount = $fObj[$k]['v'];
    else if ($fObj[$k]['k'] === 'memberRegistrationNo')
        $memberRegistrationNo = $fObj[$k]['v'];
}
?>

    <div class="content">
        <!-- Introduction -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <strong>SYSTEM SETTINGS</strong>
                </h3>
                <div id="ajax-panel"></div>
            </div>
            <div class="block-content block-content-full">
                <div id="faq1" role="tablist" aria-multiselectable="true">
                    <div class="block block-bordered block-rounded mb-5">
                        <div class="block-header bg-gray-lighter" role="tab" id="faq1_h1">
                            <a class="font-w600 text-body-color-dark" data-toggle="collapse" data-parent="#faq1"
                               href="#faq1_q1" aria-expanded="true" aria-controls="faq1_q1">
                                PAYMENT
                            </a>
                        </div>
                        <div id="faq1_q1" class="collapse show" role="tabpanel" aria-labelledby="faq1_h1">
                            <div class="block-content border-t">
                                <div class="block-content">
                                    <form class="settings-form" action="#" method="post" onsubmit="return false;">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material ">
                                                    <input type="text" class="form-control"
                                                           id="membershipNormalAmount"
                                                           name="membershipNormalAmount"
                                                           value="<? echo $membershipNormalAmount ?>">
                                                    <label for="membershipNormalAmount">Ahli Biasa (RM)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material">

                                                    <input type="text" class="form-control"
                                                           id="membershipFullAmount"
                                                           name="membershipFullAmount"
                                                           value="<? echo $membershipFullAmount ?>">
                                                    <label for="membershipFullAmount">Ahli Penuh (RM)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-alt-info">
                                                    <i class="fa fa-save mr-5"></i> Save
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block block-bordered block-rounded mb-5">
                        <div class="block-header bg-gray-lighter" role="tab" id="faq2_h1">
                            <a class="font-w600 text-body-color-dark" data-toggle="collapse" data-parent="#faq2"
                               href="#faq2_q1" aria-expanded="true" aria-controls="faq2_q1">
                                REFERENCE NO
                            </a>
                        </div>
                        <div id="faq2_q1" class="collapse show" role="tabpanel" aria-labelledby="faq2_h1">
                            <div class="block-content border-t">
                                <div class="block-content">
                                    <form class="settings-form" action="#" method="post" onsubmit="return false;">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material ">
                                                    <input type="text" class="form-control"
                                                           id="memberRegistrationNo"
                                                           name="memberRegistrationNo"
                                                           value="<? echo $memberRegistrationNo ?>">
                                                    <label for="memberRegistrationNo">No Pendaftaran Ahli</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-alt-info">
                                                    <i class="fa fa-save mr-5"></i> Save
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Introduction -->

        <!-- Functionality -->
        <!-- END Functionality -->

        <!-- Payments -->
        <!-- END Payments -->
        <!-- END Frequently Asked Questions -->
    </div>

    <!-- END Page Container -->
<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/pages/settings-form-validation.js'); ?>

    <script>
        jQuery("button[type=submit]").on('click', function () {
            jQuery.ajax({
                type: 'POST',
                url: 'SettingsUpdate.php',
                data: {
                    'membershipNormalAmount': jQuery('#membershipNormalAmount').val(),
                    'membershipFullAmount': jQuery('#membershipFullAmount').val(),
                    'memberRegistrationNo': jQuery('#memberRegistrationNo').val()
                },
                beforeSend: function () {
                    // this is where we append a loading image
                    $('#ajax-panel').html('<div class="fa fa-spin fa-gear fa-2x text-flat-dark"></div>');
                },
                success: function (data) {
                    // successful request; do something with the data
                    $('#ajax-panel').empty();
                },
                error: function () {
                    // failed request; give feedback to user
                    $('#ajax-panel').html('<p class="text-danger"><strong>Oops!</strong> Try that again in a few moments.</p>');
                }
            });
        });
    </script>

<?php require 'inc/_global/views/footer_end.php'; ?>