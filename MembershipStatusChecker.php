<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/backend_boxed/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>
<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>

    <!-- Page Content -->
    <div class="bg-corporate-darker">
        <div class="hero-static content content-full bg-white" data-toggle="appear">
            <!-- Header -->
            <div class="py-30 px-5 text-center">
                <img src="img/gema.png" alt="alternative text" title="Laman Utama" width="115" height="40" id=""
                     class="img-fluid options-item">
                <h1 class="h4 font-w700 mt-50 mb-10">Semakan Status Permohonan</h1>
                <h2 class="h5 font-w400 text-muted mb-0">Sila Masukkan No. IC Pemohon.</h2>
            </div>
            <!-- END Header -->
            <div id="not-exist-banner" class="row justify-content-center text-center" style="display: none;">
                <div class="alert alert-danger" role="alert">
                    <p class="mb-0">No. IC ini masih belum berdaftar dgn pertubuhan GEMA Malaysia.
                        <a class="alert-link"
                           href="Registration.php">Daftar sekarang</a>!
                    </p>
                </div>
            </div>
            <!-- Reminder Form -->
            <div class="row justify-content-center px-5">
                <div class="col-sm-8 col-md-6 col-xl-4">
                    <form class="js-check-form" id="checkForm" name="checkForm" method="post">
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-material floating">
                                    <input type="text" class="form-control"
                                           onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )"
                                           id="nric" name="nric" maxlength="12">
                                    <label for="nric">No. Kad Pengenalan</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit"
                                    id="checkButton"
                                    class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary">
                                Semak
                            </button>
                        </div>

                    </form>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="block" id="result-block" style="display: none;">
                        <div class="block-header bg-primary-light">
                            <h3 class="block-title">Maklumat Pendaftaran</h3>
                        </div>
                        <div class="block-content">
                            <table class="table table-borderless table-vcenter" id="result-table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="d-none d-sm-table-cell" style="width: 70%;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- dynamically inserted using ajax result-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END Page Content -->
<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/plugins/moment/moment.min.js'); ?>
    <script>
        jQuery(function () {
            jQuery('#checkForm').validate({
                errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
                },
                success: function (e) {
                    jQuery(e).closest('.form-group').removeClass('is-invalid');
                    jQuery(e).remove();
                },
                submitHandler: function (form) {
                    checkStatus();
                },
                rules: {
                    'nric': {
                        required: true,
                        minlength: 12,
                        maxlength: 12
                    }
                },
                messages: {
                    'nric': {
                        required: 'Sila masukkan no kad pengenalan',
                        minlength: 'Sila masukkan 12 digit no. kad pengenalan!',
                        maxlength: 'Sila masukkan 12 digit no. kad pengenalan!'
                    }
                }
            });
        });


        function checkStatus() {
            var submitBtn = $("button[type=submit]");
            submitBtn.prop('disabled', 'disabled');
            $('#checkButton').text('Sedang disemak...');
            var formData = {};
            formData = $('#checkForm').serialize();

            setTimeout(function () {
                $.ajax({
                    url: 'MembershipStatusValidator.php',
                    method: 'POST',
                    data: formData
                }).done(function (data, textStatus, jqXHR) {
                    // because dataType is json 'data' is guaranteed to be an object
                    console.log('done');
                    var obj = JSON.parse(data);
                    if (obj.count === 0) {
                        $('#not-exist-banner').show();
                        $('#result-block').hide();
                        $('a.alert-link').prop('href', 'Registration.php?nric=' + $('#nric').val())
                    } else {
                        res = obj.result[0];
                        $('#not-exist-banner').hide();
                        $('#result-block').show();
                        $('#result-table').html(
                            '<tr>' +
                            '<td><b>Nama</b></td><td class="d-none d-sm-table-cell">' + res.name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td><b>No. Ahli</b></td><td class="d-none d-sm-table-cell">' + res.registration_no + '</td>' +
                            '</tr>' +
							'<tr>' +
                            '<td><b>Cawangan</b></td><td class="d-none d-sm-table-cell">' + res.branch + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td><b>Pembayaran</b></td><td class="d-none d-sm-table-cell">' + (res.paid === 0 ? "Belum Dibayar" : "Telah Dibayar") + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td><b>Jenis Keahlian</b></td><td class="d-none d-sm-table-cell">' + (res.membership_type === 0 ? "Asas" : "Penuh") + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td><b>Tarikh Tamat Keahlian</b></td><td class="d-none d-sm-table-cell">' + moment(res.expired_at).format('DD-MM-YYYY') + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td><b>Kelulusan Pusat</b></td><td class="d-none d-sm-table-cell">' + res.status + '</td>' +
                            '</tr>'
                        )
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // the response is not guaranteed to be json
                    if (jqXHR.responseJSON) {
                        // jqXHR.reseponseJSON is an object
                        console.log('failed with json data');
                    }
                    else {
                        // jqXHR.responseText is not JSON data
                        console.log('failed with unknown data');
                    }
                }).always(function (dataOrjqXHR, textStatus, jqXHRorErrorThrown) {
                    console.log('always');
                    submitBtn.prop('disabled', '');
                    $('#checkButton').text('Semak');
                });
            }, 500);
        }

    </script>

<?php require 'inc/_global/views/footer_end.php'; ?>