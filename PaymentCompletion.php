<?php

require dirname(__FILE__) . '/services/RegistrationService.php';
require dirname(__FILE__) . '/services/PaymentService.php';
require dirname(__FILE__) . '/BillplzConfig.php';
require dirname(__FILE__) . '/vendor/autoload.php';

global $registrationService;
global $paymentService;

//$billId = 'j096kbaa';
$billId = $_GET['billplz']['id'];

$registration = $registrationService->getRegistrationByBillId($billId);
$bill = $paymentService->getBill($registration['branch']);
$res = $bill->get($billId);
$body = $res->getBody();
$obj = json_decode($body, true);

$registrationService->updateRegistration($billId, $obj);

if ($obj['paid']) {
    header("Location: PaymentSuccess.php?bill=" . base64_encode($obj['url']) . "&billId=" . $billId);
} else {
    header("Location: PaymentFailure.php");
}
?>