<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>
<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>
    <div id="page-container" class="page-header-modern main-content-boxed">
        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Hero -->

                <div class="block block-rounded">
                    <div class="block-content bg-pattern bg-danger-light"
                         style="background-image: url('assets/img/various/bg-pattern-inverse.png');">
                        <div class="py-20 text-center">
                            <div class="float-left">
                                <div class="fa fa-close fa-4x text-pulse"></div>
                            </div>
                            <h1 class="h3 mb-5">Your payment has been declined</h1>
                            <p>
                                Try again later. No money has been taken from your account.
                            </p>
                            <p>
                                Goto <a href="index.php">Home page</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Tasks Content -->
                <!-- END Tasks Content -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <script>
        //function redirectToReceipt() {
        //    window.location = "<?php //echo base64_decode($_GET['bill']) ?>//";
        //}
        //
        //window.body.onload = setTimeout(redirectToReceipt(), 3000);
    </script>

<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php require 'inc/_global/views/footer_end.php'; ?>