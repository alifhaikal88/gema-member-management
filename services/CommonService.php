<?php

require dirname(__FILE__) . '/../DBConn.php';

$commonService = new CommonService();

class CommonService
{
    private $pdo;

    public function __construct()
    {
        global $pdo;
        $this->pdo = $pdo;
    }

    public function getBranchById($id)
    {
        $stmt = $this->pdo->prepare('select * from g_branch where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getBranchByName($name)
    {
        $stmt = $this->pdo->prepare('select * from g_branch where name=:name ');
        $stmt->bindValue(':name', $name);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getBranches()
    {
        $query = $this->pdo->query('select * from g_branch');
        $res = $query->fetchAll();
        return $res;
    }

    public function saveBranch($newBranch)
    {
        $stmt = $this->pdo->prepare('insert into g_branch set name= :name, billplz_secret_key =:billplzSecretKey,
                                    billplz_collection_id=:billplzCollectionId');
        $stmt->execute($newBranch);
    }

    public function updateBranch($branch)
    {
        $stmt = $this->pdo->prepare('update g_branch set name= :name, billplz_secret_key =:billplzSecretKey,
                                    billplz_collection_id=:billplzCollectionId where id=:id');
        $stmt->execute($branch);
    }

    public function deleteBranch($id)
    {
        $stmt = $this->pdo->prepare('delete from g_branch where id=:id');
        $stmt->execute($id);
    }

    public function getRaceById($id)
    {
        $stmt = $this->pdo->prepare('select * from g_race where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getRaces()
    {
        $query = $this->pdo->query('select * from g_race');
        $res = $query->fetchAll();
        return $res;
    }

    public function saveRace($newRace)
    {
        $stmt = $this->pdo->prepare('insert into g_race set name= :name');
        $stmt->execute($newRace);
    }

    public function updateRace($race)
    {
        $stmt = $this->pdo->prepare('update g_race set name= :name  where id=:id');
        $stmt->execute($race);
    }

    public function deleteRace($id)
    {
        $stmt = $this->pdo->prepare('delete from g_race where id=:id');
        $stmt->execute($id);
    }
    
    public function getOccupationById($id)
    {
        $stmt = $this->pdo->prepare('select * from g_Occupation where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getOccupation()
    {
        $query = $this->pdo->query('select * from g_occupation');
        $res = $query->fetchAll();
        return $res;
    }

    public function saveOccupation($newOccupation)
    {
        $stmt = $this->pdo->prepare('insert into g_occupation set name= :name');
        $stmt->execute($newOccupation);
    }

    public function updateOccupation($occupation)
    {
        $stmt = $this->pdo->prepare('update g_occupation set name= :name  where id=:id');
        $stmt->execute($occupation);
    }

    public function deleteOccupation($id)
    {
        $stmt = $this->pdo->prepare('delete from g_occupation where id=:id');
        $stmt->execute($id);
    }
        
    public function getInterestById($id)
    {
        $stmt = $this->pdo->prepare('select * from g_interest where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getInterest()
    {
        $query = $this->pdo->query('select * from g_interest');
        $res = $query->fetchAll();
        return $res;
    }

    public function saveInterest($newInterest)
    {
        $stmt = $this->pdo->prepare('insert into g_interest set name= :name');
        $stmt->execute($newInterest);
    }

    public function updateInterest($interest)
    {
        $stmt = $this->pdo->prepare('update g_interest set name= :name  where id=:id');
        $stmt->execute($interest);
    }

    public function deleteInterest($id)
    {
        $stmt = $this->pdo->prepare('delete from g_interest where id=:id');
        $stmt->execute($id);
    }

    public function getReligionById($id)
    {
        $stmt = $this->pdo->prepare('select * from g_religion where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getReligions()
    {
        $query = $this->pdo->query('select * from g_religion');
        $res = $query->fetchAll();
        return $res;
    }

    public function saveReligion($newReligion)
    {
        $stmt = $this->pdo->prepare('insert into g_religion set name= :name');
        $stmt->execute($newReligion);
    }

    public function updateReligion($religion)
    {
        $stmt = $this->pdo->prepare('update g_religion set name= :name  where id=:id');
        $stmt->execute($religion);
    }

    public function deleteReligion($id)
    {
        $stmt = $this->pdo->prepare('delete from g_religion where id=:id');
        $stmt->execute($id);
    }
        
    public function getStaffById($id)
    {
        $stmt = $this->pdo->prepare('select * from t_staff where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }
    
     public function getStaffByUserName($username)
    {
        $stmt = $this->pdo->prepare('select * from t_staff where username=:username ');
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getStaffByName($staff_name)
    {
        $stmt = $this->pdo->prepare('select * from t_staff where staff_name=:staff_name ');
        $stmt->bindValue(':staff_name', $staff_name);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getStaff()
    {
        $query = $this->pdo->query('select * from t_staff');
        $res = $query->fetchAll();
        return $res;
    }

    public function saveStaff($newStaff)
    {
       $stmt = $this->pdo->prepare('insert into t_staff set staff_name= :staffName, staff_ic =:staffIc, staff_state=:staffState,
                                    staff_email=:staffEmail, staff_cat="Staff", user_type=2, username=:userName, password=:password');
      
       $stmt->execute($newStaff);

    }
    
    public function updateStaff($staff)
    {
       $stmt = $this->pdo->prepare('update t_staff set staff_name= :staffName, staff_ic =:staffIc, staff_state=:staffState,
                                    staff_email=:staffEmail where id=:id');
      
       $stmt->execute($staff);

    }
    
    public function updateProfile($profile)
    {
       $stmt = $this->pdo->prepare('update t_staff set password=:password, staff_name= :staffName, staff_ic =:staffIc, staff_state=:staffState,
                                    staff_email=:staffEmail where username=:username');
      
       $stmt->execute($profile);

    }

    public function deleteStaff($id)
    {
        $stmt = $this->pdo->prepare('delete from t_staff where id=:id');
        $stmt->execute($id);
    }
}
