<?php

require dirname(__FILE__) . '/../DBConn.php';
require_once dirname(__FILE__) . '/SystemService.php';
require_once dirname(__FILE__) . '/PaymentService.php';

$registrationService = new RegistrationService();

class RegistrationService
{

    private $pdo;

    /**
     * SystemService constructor.
     */
    public function __construct()
    {
        global $pdo;
        $this->pdo = $pdo;
    }

    public function getRegistrationById($id)
    {
        $stmt = $this->pdo->prepare('select * from g_registration where id=:id ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;

    }

    public function getRegistrationByBillId($billId)
    {
        global $pdo;

        $where = ['billId' => $billId];
        $stmt = $pdo->prepare('SELECT * FROM g_registration WHERE bill_id = :billId');
        $stmt->execute($where);
        return $stmt->fetch();
    }

    public function getPaidRegistrations()
    {
        $stmt = $this->pdo->prepare('select registration_no,
               approve_date registration_date,
               expired_at expired_date,
               membership_type,
               name,
               age,
               gender,
               dob,
               nric,
               phone,
               mobile,
               email,
               concat_ws(\' \', upper(address), postcode, upper(city), upper(state)) address,
               upper(occupation) occupation,
               upper(education) education,
               upper(race) race,
               upper(religion) religion,
               upper(interest) interest
               from g_registration where paid = 1 order by name ');
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function saveRegistrationFromFPX($billId)
    {
        global $pdo;

        $row = [
            'name' => $_POST['fullname'],
            'nric' => $_POST['nric'],
            'age' => $this->calculateAge($_POST['nric']),
            'dob' => $this->calculateDOB($_POST['nric']),
            'branch' => $_POST['branch'],
            'email' => $_POST['email'],
            'phone' => $_POST['phone'],
            'mobile' => $_POST['mobile'],
            'address' => $_POST['address'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'postcode' => $_POST['postcode'],
            'employerAddress' => $_POST['employer-address'],
            'education' => $_POST['education'],
            'occupation' => $_POST['occupation'],
            'race' => $_POST['race'],
            'extRace' => $_POST['inp-ext-race'],
            'religion' => $_POST['religion'],
            'extReligion' => $_POST['inp-ext-religion'],
            'gender' => $_POST['gender'],
            'interest' => json_encode($_POST['interest']),
            'membershipType' => $_POST['membershipType'],
            'billId' => $billId,
            'status' => 'PENDING',
        ];
        $sql = "INSERT INTO g_registration SET name =:name, age=:age, dob=:dob, nric=:nric, branch=:branch, email=:email, mobile=:mobile, phone=:phone,
                                               address=:address, city=:city, state=:state, postcode=:postcode, employer_address=:employerAddress,
                                               education=:education, occupation=:occupation, race=:race, ext_race=:extRace, 
                                               religion=:religion,ext_religion=:extReligion, gender=:gender, interest=:interest, 
                                               membership_type=:membershipType, bill_id=:billId, status=:status, c_ts=now() ;";
        $status = $pdo->prepare($sql)->execute($row);

        if ($status) {
            $lastId = $pdo->lastInsertId();
            echo $lastId;
        }
    }

    public function saveRegistrationFromInternal($newRegistration)
    {
        global $pdo;

        $row = [
            'name' => $_POST['fullname'],
            'nric' => $_POST['nric'],
            'age' => $this->calculateAge($_POST['nric']),
            'dob' => $this->calculateDOB($_POST['nric']),
            'branch' => $_POST['branch'],
            'email' => $_POST['email'],
            'phone' => $_POST['phone'],
            'mobile' => $_POST['mobile'],
            'address' => $_POST['address'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'postcode' => $_POST['postcode'],
            'employerAddress' => $_POST['employer-address'],
            'education' => $_POST['education'],
            'occupation' => $_POST['occupation'],
            'race' => $_POST['race'],
            'extRace' => $_POST['inp-ext-race'],
            'religion' => $_POST['religion'],
            'extReligion' => $_POST['inp-ext-religion'],
            'gender' => $_POST['gender'],
            'interest' => json_encode($_POST['interest']),
            'membershipType' => $_POST['membershipType'],
            'billId' => $billId,
            'status' => 'PENDING',
            'paid' => '1',
        ];
        $sql = "INSERT INTO g_registration SET name =:name, age=:age, dob=:dob, nric=:nric, branch=:branch, email=:email, mobile=:mobile, phone=:phone,
                                                  address=:address, city=:city, state=:state, postcode=:postcode, employer_address=:employerAddress,
                                                  education=:education, occupation=:occupation, race=:race, ext_race=:extRace, 
                                                  religion=:religion,ext_religion=:extReligion, gender=:gender, interest=:interest, 
                                                  membership_type=:membershipType, bill_id=:billId, status=:status, paid=:paid, c_ts=now() ;";
        $status = $pdo->prepare($sql)->execute($row);

        if ($status) {
            $lastId = $pdo->lastInsertId();
            echo $lastId;
        }
    }

    public function saveRegistrationFromAdmin($newManualRegistration)
    {
        global $pdo;

        $row = [
            'nomember' => $_POST['nomember'],
            'registrationDate' => $_POST['registrationDate'],
            'expiredDate' => $_POST['expiredDate'],
            'name' => $_POST['fullname'],
            'nric' => $_POST['nric'],
            'age' => $this->calculateAge($_POST['nric']),
            'dob' => $this->calculateDOB($_POST['nric']),
            'branch' => $_POST['branch'],
            'email' => $_POST['email'],
            'phone' => $_POST['phone'],
            'mobile' => $_POST['mobile'],
            'address' => $_POST['address'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'postcode' => $_POST['postcode'],
            'employerAddress' => $_POST['employer-address'],
            'education' => $_POST['education'],
            'occupation' => $_POST['occupation'],
            'race' => $_POST['race'],
            'extRace' => $_POST['inp-ext-race'],
            'religion' => $_POST['religion'],
            'extReligion' => $_POST['inp-ext-religion'],
            'gender' => $_POST['gender'],
            'interest' => json_encode($_POST['interest']),
            'membershipType' => $_POST['membershipType'],
            'activeStatus' => $_POST['activeStatus'],
            'billId' => $billId,
            'status' => 'APPROVED',
            'paid' => '1',
        ];
        $sql = "INSERT INTO g_registration SET registration_no=:nomember, approve_date=:registrationDate, expired_at=:expiredDate, name =:name, age=:age, dob=:dob, nric=:nric, branch=:branch, email=:email, mobile=:mobile, phone=:phone,
                                                  address=:address, city=:city, state=:state, postcode=:postcode, employer_address=:employerAddress,
                                                  education=:education, occupation=:occupation, race=:race, ext_race=:extRace, 
                                                  religion=:religion,ext_religion=:extReligion, gender=:gender, interest=:interest, 
                                                  membership_type=:membershipType, active=:activeStatus, bill_id=:billId, status=:status, paid=:paid, c_ts=now() ;";
        $status = $pdo->prepare($sql)->execute($row);

        if ($status) {
            $lastId = $pdo->lastInsertId();
            echo $lastId;
        }
    }

    public function updateRegistration($registrationId)
    {
        global $pdo;

        $row = [
            'registrationId' => $registrationId,
            'noMember' => $_POST['no-member'],
            'registrationDate' => $_POST['registration-date'],
            'expiredDate' => $_POST['expired-date'],
            'name' => $_POST['fullname'],
            'nric' => $_POST['nric'],
            'age' => $this->calculateAge($_POST['nric']),
            'dob' => $this->calculateDOB($_POST['nric']),
            'branch' => $_POST['branch'],
            'email' => $_POST['email'],
            'phone' => $_POST['phone'],
            'mobile' => $_POST['mobile'],
            'address' => $_POST['address'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'postcode' => $_POST['postcode'],
            'employerAddress' => $_POST['employer-address'],
            'education' => $_POST['education'],
            'occupation' => $_POST['occupation'],
            'race' => $_POST['race'],
            'extRace' => $_POST['inp-ext-race'],
            'religion' => $_POST['religion'],
            'extReligion' => $_POST['inp-ext-religion'],
            'gender' => $_POST['gender'],
            'interest' => json_encode($_POST['interest']),
            'membershipType' => $_POST['membershipType']
        ];
        $sql = 'UPDATE g_registration SET registration_no =:noMember, approve_date =:registrationDate, expired_at =:expiredDate, name =:name, age=:age, dob=:dob, nric=:nric, branch=:branch, email=:email, mobile=:mobile, phone=:phone,
                                               address=:address, city=:city, state=:state, postcode=:postcode, employer_address=:employerAddress,
                                               education=:education, occupation=:occupation, race=:race, ext_race=:extRace, 
                                               religion=:religion,ext_religion=:extReligion, gender=:gender, interest=:interest, 
                                               membership_type=:membershipType, m_ts=now() where id=:registrationId';
        $pdo->prepare($sql)->execute($row);
    }

    public function updateRegistrationBillInfo($billId, $bill)
    {
        global $pdo;

        $row = [
            'billId' => $billId,
            'paid' => $bill['paid'],
        ];
        $sql = "UPDATE g_registration SET paid=:paid WHERE bill_id=:billId;";
        $pdo->prepare($sql)->execute($row);
    }

    public function approve($id)
    {
        global $pdo;
        global $systemService;

        $where = ['id' => $id];
        $stmt = $pdo->prepare('SELECT * FROM g_registration WHERE id = :id');
        $stmt->execute($where);
        $registrationResult = $stmt->fetch();

        $expiredAt = null;
        if ($registrationResult['membership_type'] == '0') {
            $expiredAt = date('Y-m-d', strtotime('+1 year'));
        } else {
            $age = $registrationResult['age'];
            $diff = 36 - $age;
            $expiredAt = date('Y-m-d', strtotime('+' . $diff . ' year'));
        }

        $params = [
            'id' => $registrationResult['id'],
            'expiredAt' => $expiredAt,
            'approvalStatus' => 'APPROVED',
            'registrationNo' => $systemService->generateRegistrationNo()
        ];

        $sql = 'UPDATE g_registration SET registration_no = :registrationNo, status = :approvalStatus, expired_at = :expiredAt, 
                active = TRUE, m_ts = now(), approve_date = now() WHERE id = :id ';
        $pdo->prepare($sql)->execute($params);
    }

    public function reject($id)
    {
        global $pdo;
        global $systemService;

        $where = ['id' => $id];
        $stmt = $pdo->prepare('SELECT * FROM g_registration WHERE id = :id');
        $stmt->execute($where);
        $registrationResult = $stmt->fetch();

        $expiredAt = null;
        if ($registrationResult['membership_type'] == '0') {
            $expiredAt = date('Y-m-d', strtotime('+1 year'));
        } else {
            $age = $registrationResult['age'];
            $diff = 36 - $age;
            $expiredAt = date('Y-m-d', strtotime('+' . $diff . ' year'));
        }

        $params = [
            'id' => $registrationResult['id'],
            'expiredAt' => $expiredAt,
            'approvalStatus' => 'REJECTED',
            'registrationNo' => $systemService->generateRegistrationNo()
        ];

        $sql = 'UPDATE g_registration SET registration_no = :registrationNo, status = :approvalStatus, expired_at = :expiredAt, 
                active = TRUE, m_ts = now(), approve_date = now() WHERE id = :id ';
        $pdo->prepare($sql)->execute($params);
    }

   public function reconfirmPayment($id)
    {
        global $pdo;
        global $systemService;

        $where = ['id' => $id];
        $stmt = $pdo->prepare('SELECT * FROM g_registration WHERE id = :id');
        $stmt->execute($where);
        $registrationResult = $stmt->fetch();

        $expiredAt = null;
        if ($registrationResult['membership_type'] == '1') {
            $expiredAt = date('Y-m-d', strtotime('+1 year'));
        } else {
            $age = $registrationResult['age'];
            $diff = 36 - $age;
            $expiredAt = date('Y-m-d', strtotime('+' . $diff . ' year'));
        }

        $params = [
            'id' => $registrationResult['id'],
            'expiredAt' => $expiredAt,
            'approvalStatus' => 'APPROVED',
            'paid' => 1,
            'registrationNo' => $systemService->generateRegistrationNo()
        ];

        $sql = 'UPDATE g_registration SET registration_no = :registrationNo, status = :approvalStatus, paid =:paid, expired_at = :expiredAt, 
                active = TRUE, m_ts = now(), approve_date = now() WHERE id = :id ';
        $pdo->prepare($sql)->execute($params);
    }

    function calculateAge($nric)
    {
        $dob = substr($nric, 0, 6);
        echo $dob;
        echo PHP_EOL;

        $age = date_diff(DateTime::createFromFormat('ymd', $dob), date_create('now'))->y;

        echo $age;
        echo PHP_EOL;

        return $age;
    }

    function calculateDOB($nric)
    {
        $dob = substr($nric, 0, 6);
        return DateTime::createFromFormat('ymd', $dob)->format('Y-m-d');;
    }
}
