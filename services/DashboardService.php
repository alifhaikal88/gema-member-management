<?php

require dirname(__FILE__) . '/../DBConn.php';

$dashboardService = new DashboardService();

class DashboardService
{
    private $pdo;

    public function __construct()
    {
        global $pdo;
        $this->pdo = $pdo;
    }

    public function countApplication($status)
    {
        $stmt = $this->pdo->prepare('select count(*) from g_registration where status=:status');
        $stmt->execute(array('status' => $status));
        $res = $stmt->fetchColumn();
        return $res;
    }

    public function countMemberGroupByType()
    {
        $stmt = $this->pdo->prepare('select count(*) cnt, membership_type from g_registration where paid=true and status=\'APPROVED\' 
                                      group by membership_type');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getGender()
    {
        $stmt = $this->pdo->prepare('SELECT gender, COUNT(*) as number FROM g_registration GROUP BY gender');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getRaceChart()
    {
        $stmt = $this->pdo->prepare('SELECT race, COUNT(*) as number FROM g_registration GROUP BY race');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getBranchChart()
    {
        $stmt = $this->pdo->prepare('SELECT branch, COUNT(*) as number FROM g_registration GROUP BY branch');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getMembershipType()
    {
        $stmt = $this->pdo->prepare('SELECT membership_type, COUNT(*) as number FROM g_registration GROUP BY membership_type');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getStatusChart()
    {
        $stmt = $this->pdo->prepare('SELECT active, COUNT(*) as number FROM g_registration GROUP BY active');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }
}
