<?php

require dirname(__FILE__) . '/../vendor/autoload.php';
require dirname(__FILE__) . '/../BillplzConfig.php';
require dirname(__FILE__) . '/CommonService.php';
require_once dirname(__FILE__) . '/SystemService.php';

$paymentService = new  PaymentService();

class PaymentService
{

    /**
     * PaymentService constructor.
     */
    public function __construct()
    {
    }

    public function getBill($branchName)
    {
        global $commonService;
        $branch = $commonService->getBranchByName($branchName);
        $apiKey = $branch['billplz_secret_key'] === null ? DEFAULT_BILLPLZ_SECRET_KEY : $branch['billplz_secret_key'];
        $billplz = Billplz\Client::make($apiKey);
        $bill = $billplz->bill();

        return $bill;
    }

    public function createBill($membershipType, $branchName)
    {
        global $commonService;
        global $systemService;

        $branch = $commonService->getBranchByName($branchName);
        $sysConfigs = $systemService->getConfigurations();
        $filteredArr = array_filter($sysConfigs, function ($val) {
            return $val['k'] === 'membershipFullAmount' || $val['k'] === 'membershipNormalAmount';
        });

        $bill = $this->getBill($branchName);
        $collection = $branch['billplz_collection_id'] === null ? DEFAULT_BILLPLZ_COLLECTION_ID : $branch['billplz_collection_id'];

        $response = $bill->create(
            $collection,
            $_POST['email'],
            $_POST['mobile'],
            $_POST['fullname'],
            $membershipType == 0 ? $filteredArr[1]['v'] * 100 : $filteredArr[2]['v'] * 100,
            array('callback_url' => BILLPLZ_CALLBACK_URL,
                'redirect_url' => BILLPLZ_REDIRECT_URL),
            'Yuran Keahlian GEMA'
        );
        $responseArr = $response->toArray();
        $billId = $responseArr['id'];
        setcookie("billId", $billId);

        return $responseArr;
    }
}
