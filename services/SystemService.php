<?php

require dirname(__FILE__) . '/../DBConn.php';

$systemService = new  SystemService();

class SystemService
{
    private $pdo;

    public function __construct()
    {
        global $pdo;
        $this->pdo = $pdo;
    }

    public function getConfigurations()
    {
        $query = $this->pdo->query('SELECT * FROM g_configuration');
        $res = $query->fetchAll();
        return $res;
    }

    public function generateRegistrationNo()
    {
        $query = $this->pdo->query('SELECT v FROM g_configuration WHERE k = \'memberRegistrationNo\'');
        $res = $query->fetch();
        $num = $res['v'];
        $regNo = sprintf("G%'.05d\n", $num);
        $this->increaseAndSaveRegistrationNo();
        return $regNo;
    }

    public function increaseAndSaveRegistrationNo()
    {
        $query = $this->pdo->query('SELECT v FROM g_configuration WHERE k = \'memberRegistrationNo\'');
        $res = $query->fetch();
        $num = $res['v'];
        $num++;
        $stmt = $this->pdo->prepare('UPDATE g_configuration SET v = :regNo WHERE k = \'memberRegistrationNo\'');
        $params = [
            'regNo' => $num
        ];
        echo $num;
        $stmt->execute($params);
    }

}