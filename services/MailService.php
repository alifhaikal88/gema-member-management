<?php

require dirname(__FILE__) . '/../DBConn.php';
require_once dirname(__FILE__) . '/../Mail.php';

$mailService = new MailService();

class MailService
{
    private $pdo;

    public function __construct()
    {
        global $pdo;
        $this->pdo = $pdo;
    }

    public function sendMail($email, $name)
    {
        $mail = new Mail(
            $email,
            $name,
            'Pengesahan Pendaftaran Pertubuhan GEMA Malaysia',
            "");
        $mail->renderMailTemplate(dirname(__FILE__).'/../mail-template/tpl-01.html', Array('$name$' => $name));
        $mail->send();
    }
}
