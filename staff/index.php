<?php
require 'inc/_global/config.php';
require 'inc/backend/config.php';
require 'inc/_global/views/head_start.php';
require 'inc/_global/views/head_end.php';
require 'inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../services/DashboardService.php';

global $dashboardService;

$countPendingApplication = $dashboardService->countApplication('PENDING');
$countApprovedApplication = $dashboardService->countApplication('APPROVED');
$countRejectedApplication = $dashboardService->countApplication('REJECTED');
$countMemberByType = $dashboardService->countMemberGroupByType();
?>

    <!-- Page Content -->
    <div class="content">
        <div class="row gutters-tiny invisible" data-toggle="appear">
            <!-- Row #1 -->
            <div class="col-6 col-xl-4">
                <a class="block block-link-shadow text-right" href="ApplicationList.php">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="si si-people fa-3x text-body-bg-dark"></i>
                        </div>
                        <div class="font-size-h3 font-w600"><? echo $countPendingApplication; ?></div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Permohonan Baru</div>
                    </div>
                </a>
            </div>
            <div class="col-6 col-xl-4">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="si si-bag fa-3x text-body-bg-dark"></i>
                        </div>
                        <div class="font-size-h3 font-w600"><? echo $countApprovedApplication; ?></div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Permohonan Diterima</div>
                    </div>
                </a>
            </div>
            <div class="col-6 col-xl-4">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="si si-ban fa-3x text-body-bg-dark"></i>
                        </div>
                        <div class="font-size-h3 font-w600"><? echo $countRejectedApplication; ?></div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Permohonan Ditolak</div>
                    </div>
                </a>
            </div>
            <!-- END Row #1 -->
        </div>
        <div class="row gutters-tiny invisible" data-toggle="appear">
            <!-- Row #2 -->
            <div class="col-md-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">
                            Permohonan
                            <small>Bulan ini</small>
                        </h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="pull-all">
                            <!-- Lines Chart Container -->
                            <canvas class="js-chartjs-dashboard-lines"></canvas>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row items-push">
                            <div class="col-6 col-sm-4 text-center text-sm-left">
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Bulan Ini</div>
                                <div class="font-size-h4 font-w600">720</div>
                                <div class="font-w600 text-success">
                                    <i class="fa fa-caret-up"></i> +16%
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 text-center text-sm-left">
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Minggu Ini</div>
                                <div class="font-size-h4 font-w600">160</div>
                                <div class="font-w600 text-danger">
                                    <i class="fa fa-caret-down"></i> -3%
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 text-center text-sm-left">
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Purata</div>
                                <div class="font-size-h4 font-w600">24.3</div>
                                <div class="font-w600 text-success">
                                    <i class="fa fa-caret-up"></i> +9%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">
                            Kutipan Yuran
                            <small>Bulan ini</small>
                        </h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="pull-all">
                            <!-- Lines Chart Container -->
                            <canvas class="js-chartjs-dashboard-lines2"></canvas>
                        </div>
                    </div>
                    <div class="block-content bg-white">
                        <div class="row items-push">
                            <div class="col-6 col-sm-4 text-center text-sm-left">
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Bulan Ini</div>
                                <div class="font-size-h4 font-w600">RM 6,540</div>
                                <div class="font-w600 text-success">
                                    <i class="fa fa-caret-up"></i> +4%
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 text-center text-sm-left">
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Minggu Ini</div>
                                <div class="font-size-h4 font-w600">RM 1,525</div>
                                <div class="font-w600 text-danger">
                                    <i class="fa fa-caret-down"></i> -7%
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 text-center text-sm-left">
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Jumlah Kutipan</div>
                                <div class="font-size-h4 font-w600">RM 9,352</div>
                                <div class="font-w600 text-success">
                                    <i class="fa fa-caret-up"></i> +35%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <!-- Pie Chart -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Peratusan Ahli Mengikut Jenis</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-toggle="block-option"
                                    data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-center">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <!-- Pie Chart Container -->
                                <canvas class="js-chartjs-pie"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Pie Chart -->
            </div>
            <!-- END Row #2 -->
        </div>
        <div class="row gutters-tiny invisible" data-toggle="appear">
        </div>
        <div class="row gutters-tiny invisible" data-toggle="appear">
        </div>
    </div>
    <!-- END Page Content -->
<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

    <!-- Page JS Code -->
<?php $cb->get_js('js/plugins/sparkline/jquery.sparkline.min.js'); ?>
<?php $cb->get_js('js/plugins/easy-pie-chart/jquery.easypiechart.min.js'); ?>
<?php $cb->get_js('js/plugins/chartjs/Chart.bundle.min.js'); ?>
<?php $cb->get_js('js/plugins/flot/jquery.flot.min.js'); ?>
<?php $cb->get_js('js/plugins/flot/jquery.flot.pie.min.js'); ?>
<?php $cb->get_js('js/plugins/flot/jquery.flot.stack.min.js'); ?>
<?php $cb->get_js('js/plugins/flot/jquery.flot.resize.min.js'); ?>
<?php $cb->get_js('js/plugins/flot/jquery.flot.min.js'); ?>
<?php $cb->get_js('js/plugins/flot/jquery.flot.pie.min.js'); ?>
<?php $cb->get_js('js/pages/be_pages_dashboard.js'); ?>
    <script>
        jQuery(function () {
            // Init page helpers (Easy Pie Chart plugin)
            Codebase.helpers('easy-pie-chart');
            chartPolarPieDonutData['datasets'][0].data = [<?php  echo $countMemberByType[0]['cnt']?>, <? echo $countMemberByType[1]['cnt']?>];
            BePagesDashboard.init();
        });
    </script>

<?php require 'inc/_global/views/footer_end.php'; ?>