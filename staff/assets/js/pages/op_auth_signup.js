/*
 *  Document   : op_auth_signup.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Sign Up Page
 */

var OpAuthSignUp = function () {
    // Init Sign Up Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationSignUp = function () {
        jQuery('.js-validation-signup').validate({
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function (error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function (e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function (e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'signup-username': {
                    required: true,
                    minlength: 3
                },
                'nama': {
                    required: true,
                    minlength: 3
                },
                'nomykad': {
                    required: true,
                    minlength: 12,
                    maxlength: 12
                },
                'pencapaian': {
                    required: true,
                },
                'lesen': {
                    required: true,
                },
                'images': {
                    required: true,
                },
                'alamat': {
                    required: true,
                },
                'poskod': {
                    required: true,
                },
                'bandar': {
                    required: true,
                },
                'negeri': {
                    required: true,
                },
                'nohp': {
                    required: true,
                },
                'emel': {
                    required: true,
                    email: true
                },
                'pekerjaan': {
                    required: true
                },
                'cawangan': {
                    required: true
                },
                'taraf': {
                    required: true
                },
                'bangsa': {
                    required: true
                },
                'agama': {
                    required: true
                },
                'signup-terms': {
                    required: true
                }
            },
            messages: {
                'signup-username': {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'nama': {
                    required: 'Sila masukkan nama penuh',
                },
                'nomykad': {
                    required: 'Sila masukkan no mykad',
                    minlength: 'Sila masukkan 12 digit no. Mykad!',
                    maxlength: 'Sila masukkan 12 digit no. Mykad!'
                },
                'pencapaian': {
                    required: 'Sila pilih pencapaian akademik',
                },
                'lesen': {
                    required: 'Sila nyatakan ya atau tidak jika anda mempunyai lesen D',
                },
                'images': {
                    required: 'Sila muat naik salinan mykad',
                },
                'alamat': {
                    required: 'Sila masukkan alamat',
                },
                'poskod': {
                    required: 'Sila masukkan poskod',
                },
                'bandar': {
                    required: 'Sila masukkan bandar',
                },
                'negeri': {
                    required: 'Sila masukkan negeri',
                },
                'nohp': {
                    required: 'Sila masukkan no telefon',
                },

                'emel': 'Sila masukkan alamat emel yang sah',
                'signup-password': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long'
                },
                'signup-password-confirm': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above'
                },
                'signup-terms': 'You must agree to the service terms!'
            }
        });
    };

    return {
        init: function () {
            // Init SignUp Form Validation
            initValidationSignUp();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    OpAuthSignUp.init();
});