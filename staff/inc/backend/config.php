<?php
/**
 * backend/config.php
 *
 * Author: pixelcave
 *
 * Codebase - Backend papges configuration file
 *
 */

// **************************************************************************************************
// BACKEND INCLUDED VIEWS
// **************************************************************************************************

//                              : Useful for adding different sidebars/headers per page or per section
$cb->inc_side_overlay = dirname(__FILE__) . '/views/inc_side_overlay.php';
$cb->inc_sidebar = dirname(__FILE__) . '/views/inc_sidebar.php';
$cb->inc_header = dirname(__FILE__) . '/views/inc_header.php';
$cb->inc_footer = dirname(__FILE__) . '/views/inc_footer.php';


// **************************************************************************************************
// BACKEND MAIN MENU
// **************************************************************************************************

// You can use the following array to create your main menu
$cb->main_nav = array(
    array(
        'name' => '<span class="sidebar-mini-hide">Laman Utama</span>',
        'icon' => 'si si-compass',
        'url' => 'index.php'
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Permohonan Manual</span>',
        'icon' => 'si si-docs',
        'url' => 'NewRegistration.php'
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Senarai Ahli</span>',
        'icon' => 'si si-people',
        'sub' => array(
            array(
                'name' => 'Belum diluluskan',
                'url' => 'ApplicationList.php'
            ),
            array(
                'name' => 'Aktif',
                'url' => 'ActiveMemberList.php'
            ),
            array(
                'name' => 'Alumni',
                'url' => 'AlumniMemberList.php'
            ),
            array(
                'name' => 'Ditolak',
                'url' => 'RejectMemberList.php'
            )
        )
    ),
    array(
        'name' => '<span class="sidebar-mini-hide">Laporan</span>',
        'icon' => 'si si-handbag',
        'sub' => array(
            array(
                'name' => 'Tahunan',
                'url' => '#'
            ),
            array(
                'name' => 'Bulanan',
                'url' => '#'
            ),
            array(
                'name' => 'Cawangan',
                'url' => '#'
            )
        )
    )
);