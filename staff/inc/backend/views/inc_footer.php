<?php
/**
 * backend/views/inc_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page (Backend pages)
 *
 */
?>

<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="float-right">
            Sistem Pendaftaran Keahlian GEMA
        </div>
        <div class="float-left">
            <a class="font-w600" href="" target="_blank">SIGMA 1.0</a> &copy; <span class="js-year-copy">2018</span>
        </div>
    </div>
</footer>
<!-- END Footer -->
