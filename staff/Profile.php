<?php
require dirname(__FILE__) . '/../admin/inc/_global/config.php';
require dirname(__FILE__) . '/../admin/inc/backend/config.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_start.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/head_end.php';
require dirname(__FILE__) . '/../admin/inc/_global/views/page_start.php';
require dirname(__FILE__) . '/../admin/../DBConn.php';
require dirname(__FILE__) . '/../admin/../services/CommonService.php';

global $commonService;

if (isset($_GET) && isset($_GET['username'])) {
    $profile = $commonService->getStaffByUserName($_GET['username']);
}

if (isset($_POST)) {
    if (isset($_POST['submit'])) {
        $newStaff = [
            'username' => $_GET['username'],
            'staffName' => strtoupper($_POST['staffName']),
            'staffIc' => $_POST['staffIc'],
            'staffState' => $_POST['staffState'],
            'staffEmail' => $_POST['staffEmail'],
            'password' => $_POST['password'],

        ];

        $commonService->updateProfile($newStaff);
        echo "<script>window.location.href = 'index.php'</script>";

    }
}

?>
    <div class="content">
        <h2 class="content-heading">Profile</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Profile</h3>
            </div>
            <div class="block-content block-content-full">
                <form class="settings-form" action="#" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffName"
                                       name="staffName"
                                       value="<? echo $profile['staff_name'] ?>">
                                <label for="name">Nama</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffIc"
                                       name="staffIc"
                                       value="<? echo $profile['staff_ic'] ?>">
                                <label for="name">No. IC</label>
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group">
                                <div class="form-material">
                                    <select class="js-select2 form-control" id="staffState" name="staffState"
                                            style="width: 100%;" data-placeholder="Sila Pilih">
                                        <option></option>
                                        <?php
                                        global $commonService;
                                        $religions = $commonService->getBranches();
                                        foreach ($religions as $i) {
                                            $selected = '';
                                            if ($i['name'] === $profile['staff_state']) $selected = 'selected';

                                            echo '<option ' . $selected . ' value="' . $i['name'] . '">' . $i['name'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <label for="branch">Cawangan Gema *</label>
                                </div>
                            </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control text-uppercase"
                                       id="staffEmail"
                                       name="staffEmail"
                                       value="<? echo $profile['staff_email'] ?>">
                                <label for="name">Emel</label>
                            </div>
                        </div>
                    </div>					
					<div class="form-group row">
                        <div class="col-12">
                            <div class="form-material input-group">
                                <input type="password" class="form-control text-uppercase"
                                       id="password"
                                       name="password"
                                       value="<? echo $profile['password'] ?>"
                                       placeholder="">
                                <label for="name">Tukar Kata Laluan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" name="submit" class="btn btn-alt-info">
                                <i class="fa fa-save mr-5"></i> Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Dynamic Table Full -->
    </div>
    
    <!-- END Page Container -->
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/page_end.php'; ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php $cb->get_js('js/pages/race-form-validation.js'); ?>
<?php require dirname(__FILE__) . '/../admin/inc/_global/views/footer_end.php'; ?>