<?php

require dirname(__FILE__) . '/../services/CommonService.php';

global $commonService;

if (isset($_POST['submit'])) {
    $newBranch = [
        'name' => $_POST['name'],
        'billplzSecretKey' => $_POST['billplzSecretKey'],
        'billplzCollectionId' => $_POST['billplzCollectionId'],
    ];
    $commonService->saveBranch($newBranch);
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="<? echo $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="text" name="name" id="name">
    <input type="text" name="billplzSecretKey" id="billplzSecretKey">
    <input type="text" name="billplzCollectionId" id="billplzCollectionId">
    <input type="submit" value="submit" name="submit">
</form>
</body>
</html>

