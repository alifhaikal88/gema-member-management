<?php
function renderMailTemplate($tplName, $args)
{
    $tpl = file_get_contents($tplName);

    $result = preg_replace_callback(
        '~[$]\w{3,20}[$]~',
        function ($matches) use ($args) {
            foreach ($matches as $m) {
                foreach (array_keys($args) as $a) {
                    if ($a === $m) {
                        echo "ok";
                        return str_replace($a, $args[$a], $m);
                    }
                }
            }
            return null;
        },
        $tpl);
    echo $result;
}

renderMailTemplate('../mail-template/tpl-01.html', Array('$name$' => 'alif haikal razak'));


