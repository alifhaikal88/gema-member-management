<?php

require dirname(__FILE__) . '/services/RegistrationService.php';
require dirname(__FILE__) . '/services/MailService.php';
require 'inc/_global/config.php';
require 'inc/_global/views/head_start.php';
require 'inc/_global/views/head_end.php';
require 'inc/_global/views/page_start.php';

global $registrationService;
global $mailService;

?>
    <div id="page-container" class="page-header-modern main-content-boxed">
        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Hero -->
                <div class="block block-rounded">
                    <div class="block-content bg-pattern bg-earth-lighter"
                         style="background-image: url('assets/img/various/bg-pattern-inverse.png');">
                        <div class="py-20 text-center">
                            <div class="float-left">
                                <div class="fa fa-check fa-4x text-success"></div>
                            </div>
                            <h1 class="h3 mb-5">Terima kasih!</h1>
                            <p>
                                Pendaftaran anda telah diterima dan akan diproses
                            </p>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Tasks Content -->
                <!-- END Tasks Content -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>
    <!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?php require 'inc/_global/views/footer_end.php'; ?>
<?php
$res = $registrationService->getRegistrationByBillId($_GET['billId']);
$mailService->sendMail($res['email'], $res['name']);
?>