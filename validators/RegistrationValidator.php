<?php

require dirname(__FILE__) . '/../DBConn.php';

$nric = $_POST['nric'];

$stmt = $pdo->prepare('select * from g_registration g where g.nric = :nric and g.paid = true');
$stmt->bindValue(':nric', $nric);
$stmt->execute();
$result = $stmt->fetchAll();
$count = count($result);

if ($result[0]['status'] === 'PENDING') {
    echo json_encode($count > 0 ? "No IC ini telah didaftarkan dan belum diluluskan di bahagian pusat" : true);
} else {
    echo json_encode($count > 0 ? false : true);
}
