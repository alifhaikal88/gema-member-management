<?php

spl_autoload_extensions(".php");
spl_autoload_register();

global $utilService;

$age = $_POST['age'];

if ($age < 18 || $age > 36) {
    echo json_encode('Umur mestilah antara 18 hingga 36 tahun');
} else {
    echo json_encode(true);
}
